module.exports = {
  type: process.env.DB_TYPE,
  host: process.env.DB_HOST,
  port: parseInt(process.env.DB_PORT),
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  entities: JSON.parse(process.env.DB_ENTITIES),
  migrations: JSON.parse(process.env.DB_MIGRATIONS),
  synchronize: process.env.DB_SYNC === 'true',
  verboseRetryLog: true,
  autoLoadEntities: true,
  logging: true,
};
