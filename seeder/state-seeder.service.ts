import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { State } from '../src/state/state.entity';
import { Repository } from 'typeorm';

@Injectable()
export class StateSeederService {
  constructor(
    @InjectRepository(State)
    public states: Repository<State>,
  ) {
    //
  }

  async run() {
    if ((await this.states.count()) !== 0) {
      return;
    }

    const stateList = [];
    stateList.push(this.states.create({ id: 1, name: 'آذربایجان شرقی' }));
    stateList.push(this.states.create({ id: 2, name: 'آذربایجان غربی' }));
    stateList.push(this.states.create({ id: 3, name: 'اردبیل' }));
    stateList.push(this.states.create({ id: 4, name: 'اصفهان' }));
    stateList.push(this.states.create({ id: 5, name: 'البرز' }));
    stateList.push(this.states.create({ id: 6, name: 'ایلام' }));
    stateList.push(this.states.create({ id: 7, name: 'بوشهر' }));
    stateList.push(this.states.create({ id: 8, name: 'تهران' }));
    stateList.push(this.states.create({ id: 9, name: 'چهارمحال وبختیاری' }));
    stateList.push(this.states.create({ id: 10, name: 'خراسان جنوبی' }));
    stateList.push(this.states.create({ id: 11, name: 'خراسان رضوی' }));
    stateList.push(this.states.create({ id: 12, name: 'خراسان شمالی' }));
    stateList.push(this.states.create({ id: 13, name: 'خوزستان' }));
    stateList.push(this.states.create({ id: 14, name: 'زنجان' }));
    stateList.push(this.states.create({ id: 15, name: 'سمنان' }));
    stateList.push(this.states.create({ id: 16, name: 'سیستان وبلوچستان' }));
    stateList.push(this.states.create({ id: 17, name: 'فارس' }));
    stateList.push(this.states.create({ id: 18, name: 'قزوین' }));
    stateList.push(this.states.create({ id: 19, name: 'قم' }));
    stateList.push(this.states.create({ id: 20, name: 'کردستان' }));
    stateList.push(this.states.create({ id: 21, name: 'کرمان' }));
    stateList.push(this.states.create({ id: 22, name: 'کرمانشاه' }));
    stateList.push(this.states.create({ id: 23, name: 'کهگیلویه وبویراحمد' }));
    stateList.push(this.states.create({ id: 24, name: 'گلستان' }));
    stateList.push(this.states.create({ id: 25, name: 'گیلان' }));
    stateList.push(this.states.create({ id: 26, name: 'لرستان' }));
    stateList.push(this.states.create({ id: 27, name: 'مازندران' }));
    stateList.push(this.states.create({ id: 28, name: 'مرکزی' }));
    stateList.push(this.states.create({ id: 29, name: 'هرمزگان' }));
    stateList.push(this.states.create({ id: 30, name: 'همدان' }));
    stateList.push(this.states.create({ id: 31, name: 'یزد' }));

    await this.states.save(stateList);
  }
}
