import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Admin } from '../src/admin/admin.entity';
import { Repository } from 'typeorm';
import { hash } from 'argon2';

@Injectable()
export class SeederService {
  constructor(
    @InjectRepository(Admin)
    public admins: Repository<Admin>,
  ) {
    //
  }

  async run() {
    const adminCount = await this.admins.count();
    if (adminCount !== 0) {
      return;
    }

    const admin1 = this.admins.create({
      phone: '09033669998',
      password: await hash('12345678'),
      fullName: 'AshkanAbd',
    });
    const admin2 = this.admins.create({
      phone: '09358104719',
      password: await hash('12345678'),
      fullName: 'Mahmud Parande',
    });

    await this.admins.save([admin1, admin2]);
  }
}
