import { NestFactory } from '@nestjs/core';
import { SeederService } from './seeder.service';
import { AppModule } from './app.module';
import * as dotenv from 'dotenv';
import { StateSeederService } from './state-seeder.service';
import { CitySeederService } from './city-seeeder.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const stateSeeder = app.get<StateSeederService>(StateSeederService);
  await stateSeeder.run();

  const citySeeder = app.get<CitySeederService>(CitySeederService);
  await citySeeder.run();

  const seeder = app.get<SeederService>(SeederService);
  await seeder.run();
  await app.close();
  process.exit(0);
}

dotenv.config();
bootstrap();
