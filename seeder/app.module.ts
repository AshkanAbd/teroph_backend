import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Entities } from '../src/app.entities';
import { SeederService } from './seeder.service';
import * as dotenv from 'dotenv';
import { StateSeederService } from './state-seeder.service';
import { CitySeederService } from './city-seeeder.service';

dotenv.config();

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: process.env.DB_TYPE as 'postgres',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT),
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      entities: JSON.parse(process.env.DB_ENTITIES),
      migrations: JSON.parse(process.env.DB_MIGRATIONS),
      synchronize: process.env.DB_SYNC === 'true',
      verboseRetryLog: true,
      autoLoadEntities: true,
    }),
    TypeOrmModule.forFeature([...Entities]),
  ],
  providers: [SeederService, StateSeederService, CitySeederService],
})
export class AppModule {
  //
}
