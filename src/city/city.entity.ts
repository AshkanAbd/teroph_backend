import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, RelationId } from 'typeorm';
import { State } from '../state/state.entity';
import { Patient } from '../patient/patient.entity';

@Entity('cities')
export class City {
  @PrimaryGeneratedColumn('increment', { type: 'bigint' })
  id: number;
  @Column({ type: 'varchar', length: 200, nullable: false })
  name: string;
  @RelationId((object: City) => object.state)
  @Column({ type: 'bigint', name: 'state_id' })
  stateId: number;
  @ManyToOne(() => State, (state) => state.cities)
  @JoinColumn({ name: 'state_id' })
  state: State;
  @OneToMany(() => Patient, (patient) => (patient.city, patient.cityId), {
    cascade: true,
    nullable: false,
    onDelete: 'CASCADE',
  })
  patients: Patient[];
}
