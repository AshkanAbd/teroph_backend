export class GetCityListQuery {
  page: number;
  pageSize: number;
  stateId: string;
  search: string;

  constructor(page: number, pageSize: number, stateId: string, search: string) {
    this.page = page;
    this.pageSize = pageSize;
    this.stateId = stateId;
    this.search = search;
  }
}
