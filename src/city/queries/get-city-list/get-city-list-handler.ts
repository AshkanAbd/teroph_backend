import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { StdResponse } from '../../../common/std-response/std-response';
import { PaginationDto } from '../../../common/pagination/pagination.dto';
import { GetCityListDto } from './get-city-list-dto';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { GetCityListQuery } from './get-city-list-query';
import { Ok } from '../../../common/std-response/std-reponse.factory';

@QueryHandler(GetCityListQuery)
export class GetCityListHandler
  extends AbstractHandler
  implements IQueryHandler<GetCityListQuery, StdResponse<PaginationDto<GetCityListDto>>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(query: GetCityListQuery): Promise<StdResponse<PaginationDto<GetCityListDto>>> {
    let queryBuilder = this.repositoryService.cities.createQueryBuilder();

    if (!!query.search) {
      query.search = query.search.toLowerCase();

      queryBuilder = queryBuilder.where(`name ilike :search`, { search: `%${query.search}%` });
    }

    if (!!parseInt(query.stateId)) {
      queryBuilder = queryBuilder.where(`state_id = :state_id`, { state_id: query.stateId });
    }

    const rawCities = await queryBuilder.pagination({
      page: query.page,
      pageSize: query.pageSize,
    });

    const cities = new PaginationDto<GetCityListDto>({
      pageCount: rawCities.pageCount,
      currentPage: rawCities.currentPage,
      currentPageSize: rawCities.currentPageSize,
      total: rawCities.total,
    });

    cities.list = rawCities.list.map((x) => {
      return new GetCityListDto(x.id, x.name);
    });

    return Ok(cities);
  }
}
