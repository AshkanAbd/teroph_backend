import { GetCityListHandler } from './get-city-list/get-city-list-handler';

export const QueryHandlers = [GetCityListHandler];
