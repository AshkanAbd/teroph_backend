import { Controller, Get, Query, Res } from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { ApiPaginationQuery } from '../common/swagger/ApiPaginationQuery';
import { ApiSearchQuery } from '../common/swagger/ApiSearchQuery';
import { ApiPaginationResponse } from '../common/swagger/ApiPaginationResponse';
import { GetCityListDto } from './queries/get-city-list/get-city-list-dto';
import { Response } from 'express';
import { QueryBus } from '@nestjs/cqrs';
import { AbstractController } from '../common/extensions/abstract-controller';
import { GetCityListQuery } from './queries/get-city-list/get-city-list-query';

@ApiTags('city')
@Controller('city')
export class CityController extends AbstractController {
  constructor(private queryBus: QueryBus) {
    super();
  }

  @Get()
  @ApiPaginationQuery()
  @ApiQuery({ required: false, name: 'stateId' })
  @ApiSearchQuery()
  @ApiPaginationResponse(GetCityListDto)
  async getCityList(
    @Query('search') search: string,
    @Query('stateId') stateId: string,
    @Query('page') page: number,
    @Query('pageSize') pageSize: number,
    @Res() res: Response,
  ) {
    this.base(res, await this.queryBus.execute(new GetCityListQuery(page, pageSize, stateId, search)));
  }
}
