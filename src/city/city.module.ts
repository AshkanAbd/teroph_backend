import { Module } from '@nestjs/common';
import { CityController } from './city.controller';
import { AppRepositoryService } from '../services/app-repository.service';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Entities } from '../app.entities';
import { QueryHandlers } from './queries';

@Module({
  imports: [TypeOrmModule.forFeature([...Entities]), CqrsModule],
  controllers: [CityController],
  providers: [AppRepositoryService, ...QueryHandlers],
})
export class CityModule {
  //
}
