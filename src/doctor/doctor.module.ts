import { Module } from '@nestjs/common';
import { DoctorController } from './doctor.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CqrsModule } from '@nestjs/cqrs';
import { AppRepositoryService } from '../services/app-repository.service';
import { Entities } from '../app.entities';
import { QueryHandlers } from './queries';
import { CommandHandlers } from './commands';

@Module({
  imports: [TypeOrmModule.forFeature([...Entities]), CqrsModule],
  controllers: [DoctorController],
  providers: [AppRepositoryService, ...QueryHandlers, ...CommandHandlers],
})
export class DoctorModule {
  //
}
