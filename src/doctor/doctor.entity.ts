import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Visit } from '../visit/visit.entity';

@Entity('doctors')
export class Doctor {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'id' })
  public id: number;
  @Column({ type: 'text', name: 'full_name', nullable: false })
  public fullName: string;
  @Column({ type: 'text', name: 'medical_number', nullable: false })
  public medicalNumber: string;
  @Column({ type: 'text', name: 'speciality', nullable: false, default: '' })
  public speciality: string;
  @Column({ type: 'text', name: 'referring_doctor', nullable: true })
  public referringDoctor: string;
  @Column({ type: 'text', name: 'referring_medical_number', nullable: true })
  public referringDoctorMedicalNumber: string;
  @Column({ type: 'text', name: 'referring_doctor_hospital', nullable: true })
  public referringDoctorHospital: string;
  @CreateDateColumn({ type: 'timestamp', name: 'created_at', nullable: false })
  public createdAt: Date;
  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at', nullable: false })
  public updatedAt: Date;
  @DeleteDateColumn({ type: 'timestamp', name: 'deleted_at', nullable: true })
  public deletedAt: Date;
  @OneToMany(() => Visit, (visit) => visit.doctor, {
    cascade: true,
    nullable: false,
    onDelete: 'CASCADE',
  })
  visits: Visit[];
}
