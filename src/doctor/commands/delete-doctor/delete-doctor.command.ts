export class DeleteDoctorCommand {
  id: number;

  constructor(id: number) {
    this.id = id;
  }
}
