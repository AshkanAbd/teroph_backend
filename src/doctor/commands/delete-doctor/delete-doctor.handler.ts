import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { DeleteDoctorCommand } from './delete-doctor.command';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { PaginationDto } from '../../../common/pagination/pagination.dto';
import { GetDoctorDto } from '../../queries/get-doctor/get-doctor.dto';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { GetDoctorListHandler } from '../../queries/get-doctor-list/get-doctor-list.handler';
import { GetDoctorListQuery } from '../../queries/get-doctor-list/get-doctor-list.query';
import { NotFound } from '../../../common/std-response/std-reponse.factory';

@CommandHandler(DeleteDoctorCommand)
export class DeleteDoctorHandler
  extends AbstractHandler
  implements ICommandHandler<DeleteDoctorCommand, StdResponse<PaginationDto<GetDoctorDto>>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(command: DeleteDoctorCommand): Promise<StdResponse<PaginationDto<GetDoctorDto>>> {
    const doctor = await this.repositoryService.doctors.findOne(command.id);

    if (!doctor) {
      return NotFound();
    }

    await this.repositoryService.doctors.softRemove(doctor);

    const getDoctorListHandler = new GetDoctorListHandler(this.repositoryService, this.moduleRef);
    const getDoctorList = await getDoctorListHandler.execute(new GetDoctorListQuery(null, 1, 10));
    getDoctorList.message = 'Doctor deleted successfully.';
    return getDoctorList;
  }
}
