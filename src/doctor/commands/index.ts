import { CreateDoctorHandler } from './create-doctor/create-doctor.handler';
import { UpdateDoctorHandler } from './update-doctor/update-doctor.handler';
import { DeleteDoctorHandler } from './delete-doctor/delete-doctor.handler';

export const CommandHandlers = [CreateDoctorHandler, UpdateDoctorHandler, DeleteDoctorHandler];
