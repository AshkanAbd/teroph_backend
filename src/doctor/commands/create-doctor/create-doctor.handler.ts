import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { CreateDoctorCommand } from './create-doctor.command';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { BadRequest } from '../../../common/std-response/std-reponse.factory';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { GetDoctorDto } from '../../queries/get-doctor/get-doctor.dto';
import { Validator } from '../../../common/validations/validator';
import { GetDoctorHandler } from '../../queries/get-doctor/get-doctor.handler';
import { GetDoctorQuery } from '../../queries/get-doctor/get-doctor.query';

@CommandHandler(CreateDoctorCommand)
export class CreateDoctorHandler
  extends AbstractHandler
  implements ICommandHandler<CreateDoctorCommand, StdResponse<GetDoctorDto>>
{
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(command: CreateDoctorCommand): Promise<StdResponse<GetDoctorDto>> {
    const validationResult = await Validator.validate(command);
    if (validationResult.failed()) {
      return BadRequest<any>(validationResult.getMessage());
    }

    let doctor = this.repositoryService.doctors.create({
      fullName: command.fullName,
      medicalNumber: command.medicalNumber,
      speciality: command.speciality,
      referringDoctor: command.referringDoctor,
      referringDoctorMedicalNumber: command.referringDoctorMedicalNumber,
      referringDoctorHospital: command.referringDoctorHospital,
    });

    doctor = await this.repositoryService.doctors.save(doctor);

    const getDoctorHandler = new GetDoctorHandler(this.repositoryService, this.moduleRef);
    const getDoctor = await getDoctorHandler.execute(new GetDoctorQuery(doctor.id));
    getDoctor.message = 'Doctor created successfully.';
    return getDoctor;
  }
}
