import {
  IsNotEmpty,
  IsNumberString,
  isString,
  IsString,
  maxLength,
  MaxLength,
  minLength,
  MinLength,
} from 'class-validator';
import { Must, NotNullAsync } from '../../../common/validations/custom-validation/must.rule';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ApiProperty } from '@nestjs/swagger';
import { MustSync, NotNull } from '../../../common/validations/custom-validation/must-sync.rule';

export class UpdateDoctorCommand {
  id: number;

  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  @MinLength(1, { message: '' })
  @MaxLength(1000, { message: '' })
  @ApiProperty()
  public fullName: string;

  @IsNotEmpty({ message: '' })
  @IsNumberString(null, { message: 'Medical number should be number.' })
  @MinLength(1, { message: '' })
  @MaxLength(1000, { message: '' })
  @Must(
    async (moduleRef, property, object) => {
      const repositories = await moduleRef.resolve(AppRepositoryService);
      return (
        (await repositories.doctors
          .createQueryBuilder()
          .where('medical_number = :medical_number', { medical_number: property })
          .andWhere('id != :id', { id: object.id })
          .getCount()) === 0
      );
    },
    NotNullAsync,
    { message: 'Medical number exists.' },
  )
  @ApiProperty()
  public medicalNumber: string;

  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  @MinLength(1, { message: '' })
  @MaxLength(1000, { message: '' })
  @ApiProperty()
  public speciality: string;

  @MustSync((_, x) => isString(x), NotNull, { message: '' })
  @MustSync((_, x) => minLength(x, 1), NotNull, { message: '' })
  @MustSync((_, x) => maxLength(x, 1000), NotNull, { message: '' })
  @ApiProperty()
  public referringDoctor: string;

  @MustSync((_, x) => isString(x), NotNull, { message: '' })
  @MustSync((_, x) => minLength(x, 1), NotNull, { message: '' })
  @MustSync((_, x) => maxLength(x, 1000), NotNull, { message: '' })
  @ApiProperty()
  public referringDoctorMedicalNumber: string;

  @MustSync((_, x) => isString(x), NotNull, { message: '' })
  @MustSync((_, x) => minLength(x, 1), NotNull, { message: '' })
  @MustSync((_, x) => maxLength(x, 1000), NotNull, { message: '' })
  @ApiProperty()
  public referringDoctorHospital: string;
}
