import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { UpdateDoctorCommand } from './update-doctor.command';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { GetDoctorDto } from '../../queries/get-doctor/get-doctor.dto';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { Validator } from '../../../common/validations/validator';
import { BadRequest, NotFound } from '../../../common/std-response/std-reponse.factory';
import { GetDoctorHandler } from '../../queries/get-doctor/get-doctor.handler';
import { GetDoctorQuery } from '../../queries/get-doctor/get-doctor.query';

@CommandHandler(UpdateDoctorCommand)
export class UpdateDoctorHandler
  extends AbstractHandler
  implements ICommandHandler<UpdateDoctorCommand, StdResponse<GetDoctorDto>>
{
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(command: UpdateDoctorCommand): Promise<StdResponse<GetDoctorDto>> {
    const doctor = await this.repositoryService.doctors.findOne(command.id);

    if (!doctor) {
      return NotFound();
    }

    const validationResult = await Validator.validate(command);
    if (validationResult.failed()) {
      return BadRequest<any>(validationResult.getMessage());
    }

    doctor.fullName = command.fullName;
    doctor.medicalNumber = command.medicalNumber;
    doctor.referringDoctor = command.referringDoctor;
    doctor.speciality = command.speciality;
    doctor.referringDoctorHospital = command.referringDoctorHospital;
    doctor.referringDoctorMedicalNumber = command.referringDoctorMedicalNumber;

    await this.repositoryService.doctors.save(doctor);

    const getDoctorHandler = new GetDoctorHandler(this.repositoryService, this.moduleRef);
    const getDoctor = await getDoctorHandler.execute(new GetDoctorQuery(command.id));
    getDoctor.message = 'Doctor updated successfully.';
    return getDoctor;
  }
}
