import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Res,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { Response } from 'express';
import { GetDoctorListQuery } from './queries/get-doctor-list/get-doctor-list.query';
import { AbstractController } from '../common/extensions/abstract-controller';
import { ApiPaginationResponse } from '../common/swagger/ApiPaginationResponse';
import { ApiPaginationQuery } from '../common/swagger/ApiPaginationQuery';
import { ApiSearchQuery } from '../common/swagger/ApiSearchQuery';
import { GetDoctorDto } from './queries/get-doctor/get-doctor.dto';
import { GetDoctorQuery } from './queries/get-doctor/get-doctor.query';
import { NotFoundStdException } from '../common/std-response/std-reponse.factory';
import { CreateDoctorCommand } from './commands/create-doctor/create-doctor.command';
import { TransformerPipe } from '../common/pipes/transformer.pipe';
import { UpdateDoctorCommand } from './commands/update-doctor/update-doctor.command';
import { DeleteDoctorCommand } from './commands/delete-doctor/delete-doctor.command';
import { AdminAuthGuard } from '../auth/admin/admin-auth.guard';

@ApiBearerAuth()
@ApiTags('doctor')
@Controller('doctor')
@UseGuards(AdminAuthGuard)
export class DoctorController extends AbstractController {
  constructor(private queryBus: QueryBus, private commandBus: CommandBus) {
    super();
  }

  @Get()
  @ApiPaginationQuery()
  @ApiSearchQuery()
  @ApiPaginationResponse(GetDoctorDto)
  async getDoctorList(
    @Query('search') search: string,
    @Query('page') page: number,
    @Query('pageSize') pageSize: number,
    @Res() res: Response,
  ) {
    this.base(res, await this.queryBus.execute(new GetDoctorListQuery(search, page, pageSize)));
  }

  @Post()
  @ApiBody({ type: CreateDoctorCommand })
  @ApiResponse({ type: GetDoctorDto })
  @UsePipes(new TransformerPipe('body'))
  async createDoctor(@Body() createDoctorCommand: CreateDoctorCommand, @Res() res: Response) {
    this.base(res, await this.commandBus.execute(createDoctorCommand));
  }

  @Get('/:id')
  @ApiParam({ name: 'id', type: Number, required: true })
  @ApiResponse({ type: GetDoctorDto })
  async getDoctor(
    @Param('id', new ParseIntPipe({ exceptionFactory: NotFoundStdException })) id: number,
    @Res() res: Response,
  ) {
    this.base(res, await this.queryBus.execute(new GetDoctorQuery(id)));
  }

  @Put('/:id')
  @ApiParam({ name: 'id', type: Number, required: true })
  @ApiBody({ type: UpdateDoctorCommand })
  @ApiResponse({ type: GetDoctorDto })
  @UsePipes(new TransformerPipe('body'))
  async updateDoctor(
    @Param('id', new ParseIntPipe({ exceptionFactory: NotFoundStdException })) id: number,
    @Body() updateDoctorCommand: UpdateDoctorCommand,
    @Res() res: Response,
  ) {
    updateDoctorCommand.id = id;
    this.base(res, await this.commandBus.execute(updateDoctorCommand));
  }

  @Delete('/:id')
  @ApiParam({ name: 'id', type: Number, required: true })
  @ApiPaginationResponse(GetDoctorDto)
  async deleteDoctor(
    @Param('id', new ParseIntPipe({ exceptionFactory: NotFoundStdException })) id: number,
    @Res() res: Response,
  ) {
    this.base(res, await this.commandBus.execute(new DeleteDoctorCommand(id)));
  }
}
