import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetDoctorListQuery } from './get-doctor-list.query';
import { StdResponse } from '../../../common/std-response/std-response';
import { PaginationDto } from '../../../common/pagination/pagination.dto';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { Ok } from '../../../common/std-response/std-reponse.factory';
import { Brackets } from 'typeorm';
import { GetDoctorDto } from '../get-doctor/get-doctor.dto';

@QueryHandler(GetDoctorListQuery)
export class GetDoctorListHandler
  extends AbstractHandler
  implements IQueryHandler<GetDoctorListQuery, StdResponse<PaginationDto<GetDoctorDto>>>
{
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(query: GetDoctorListQuery): Promise<StdResponse<PaginationDto<GetDoctorDto>>> {
    let queryBuilder = this.repositoryService.doctors.createQueryBuilder().orderBy('created_at', 'DESC');

    if (!!query.search) {
      query.search = query.search.toLowerCase();
      queryBuilder = queryBuilder.where(
        new Brackets((q) => {
          q.where('full_name ilike :search', { search: `%${query.search}%` }).orWhere('medical_number ilike :search', {
            search: `%${query.search}%`,
          });
        }),
      );
    }

    const rawDoctors = await queryBuilder.pagination({
      page: query.page,
      pageSize: query.pageSize,
    });

    const doctors = new PaginationDto<GetDoctorDto>({
      currentPage: rawDoctors.currentPage,
      currentPageSize: rawDoctors.currentPage,
      pageCount: rawDoctors.currentPage,
      total: rawDoctors.currentPage,
    });

    doctors.list = rawDoctors.list.map((x) => {
      return new GetDoctorDto({
        id: x.id,
        fullName: x.fullName,
        medicalNumber: x.medicalNumber,
        referringDoctor: x.referringDoctor,
        referringDoctorHospital: x.referringDoctorHospital,
        referringDoctorMedicalNumber: x.referringDoctorMedicalNumber,
        speciality: x.speciality,
      });
    });

    return Ok(doctors);
  }
}
