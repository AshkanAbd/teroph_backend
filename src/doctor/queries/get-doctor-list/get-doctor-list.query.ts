export class GetDoctorListQuery {
  public search: string;
  public page: number;
  public pageSize: number;

  constructor(search: string, page: number, pageSize: number) {
    this.search = search;
    this.page = page;
    this.pageSize = pageSize;
  }
}
