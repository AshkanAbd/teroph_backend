import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetDoctorQuery } from './get-doctor.query';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { GetDoctorDto } from './get-doctor.dto';
import { NotFoundMsg, Ok } from '../../../common/std-response/std-reponse.factory';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';

@QueryHandler(GetDoctorQuery)
export class GetDoctorHandler
  extends AbstractHandler
  implements IQueryHandler<GetDoctorQuery, StdResponse<GetDoctorDto>>
{
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(query: GetDoctorQuery): Promise<StdResponse<GetDoctorDto>> {
    const doctor = await this.repositoryService.doctors.findOne(query.id);

    if (!doctor) {
      return NotFoundMsg();
    }

    return Ok(
      new GetDoctorDto({
        id: doctor.id,
        fullName: doctor.fullName,
        medicalNumber: doctor.medicalNumber,
        referringDoctor: doctor.referringDoctor,
        referringDoctorHospital: doctor.referringDoctorHospital,
        referringDoctorMedicalNumber: doctor.referringDoctorMedicalNumber,
        speciality: doctor.speciality,
      }),
    );
  }
}
