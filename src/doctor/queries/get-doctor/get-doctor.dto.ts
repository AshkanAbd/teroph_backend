import { ApiProperty } from '@nestjs/swagger';

export class GetDoctorDto {
  @ApiProperty()
  public id: number;
  @ApiProperty()
  public fullName: string;
  @ApiProperty()
  public medicalNumber: string;
  @ApiProperty()
  public speciality: string;
  @ApiProperty()
  public referringDoctor: string;
  @ApiProperty()
  public referringDoctorMedicalNumber: string;
  @ApiProperty()
  public referringDoctorHospital: string;

  constructor(init?: GetDoctorDto) {
    Object.assign(this, init);
  }
}
