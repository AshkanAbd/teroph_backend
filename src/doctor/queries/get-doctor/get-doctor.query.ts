export class GetDoctorQuery {
  public id: number;

  constructor(id: number) {
    this.id = id;
  }
}
