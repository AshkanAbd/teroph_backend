import { GetDoctorListHandler } from './get-doctor-list/get-doctor-list.handler';
import { GetDoctorHandler } from './get-doctor/get-doctor.handler';

export const QueryHandlers = [GetDoctorListHandler, GetDoctorHandler];
