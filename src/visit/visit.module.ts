import { Module } from '@nestjs/common';
import { VisitController } from './visit.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Entities } from '../app.entities';
import { CqrsModule } from '@nestjs/cqrs';
import { AppRepositoryService } from '../services/app-repository.service';
import { CommandHandlers } from './commands';
import { QueryHandlers } from './queries';

@Module({
  imports: [TypeOrmModule.forFeature([...Entities]), CqrsModule],
  controllers: [VisitController],
  providers: [AppRepositoryService, ...CommandHandlers, ...QueryHandlers],
})
export class VisitModule {
  //
}
