import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { DeleteVisitCommand } from './delete-visit.command';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { PaginationDto } from '../../../common/pagination/pagination.dto';
import { GetVisitListDto } from '../../queries/get-visit-list/get-visit-list.dto';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { NotFound } from '../../../common/std-response/std-reponse.factory';
import { GetVisitListHandler } from '../../queries/get-visit-list/get-visit-list.handler';
import { GetVisitListQuery } from '../../queries/get-visit-list/get-visit-list.query';

@CommandHandler(DeleteVisitCommand)
export class DeleteVisitHandler
  extends AbstractHandler
  implements ICommandHandler<DeleteVisitCommand, StdResponse<PaginationDto<GetVisitListDto>>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(command: DeleteVisitCommand): Promise<StdResponse<PaginationDto<GetVisitListDto>>> {
    const visit = await this.repositoryService.visits.findOne(command.id);
    if (!visit) {
      return NotFound();
    }

    await this.repositoryService.visits.softRemove(visit);

    const getVisitListHandler = new GetVisitListHandler(this.repositoryService, this.moduleRef);
    return getVisitListHandler.execute(new GetVisitListQuery(null, null, 1, 10));
  }
}
