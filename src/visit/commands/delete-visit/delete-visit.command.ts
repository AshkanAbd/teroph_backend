export class DeleteVisitCommand {
  id: number;

  constructor(id: number) {
    this.id = id;
  }
}
