import { ApiProperty } from '@nestjs/swagger';
import {
  ArrayMinSize,
  ArrayUnique,
  IsArray,
  IsBoolean,
  IsBooleanString,
  isDateString,
  IsDateString,
  isInt,
  IsInt,
  IsNotEmpty,
  isString,
  IsString,
} from 'class-validator';
import { Must, NotNullAsync } from '../../../common/validations/custom-validation/must.rule';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { MustSync, NotNull } from '../../../common/validations/custom-validation/must-sync.rule';

export class UpdateVisitCommand {
  id: number;

  @ApiProperty()
  @IsNotEmpty({ message: '' })
  @IsInt({ message: '' })
  @Must(
    async (moduleRef, property) => {
      const repository = await moduleRef.resolve(AppRepositoryService);
      return !!(await repository.patients.findOne(property));
    },
    NotNullAsync,
    { message: '' },
  )
  patientId: number;
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  @IsInt({ message: '' })
  @Must(
    async (moduleRef, property) => {
      const repository = await moduleRef.resolve(AppRepositoryService);
      return !!(await repository.doctors.findOne(property));
    },
    NotNullAsync,
    { message: '' },
  )
  doctorId: number;
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  @IsInt({ message: '' })
  gravida: number;
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  @IsInt({ message: '' })
  para: number;
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  @IsBoolean({ message: '' })
  twin: boolean;
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  numberOfLiveBirth: string;
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  abortion: string;
  @ApiProperty()
  @MustSync((_, x) => isString(x), NotNull, { message: '' })
  causeOfAbortion: string;
  @ApiProperty()
  @MustSync(
    (_, x) => {
      if (!x) {
        return true;
      }
      return isDateString(x);
    },
    NotNull,
    { message: '' },
  )
  lmp: Date;
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  @IsInt({ message: '' })
  gestationalAgeWeeks: number;
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  @IsInt({ message: '' })
  gestationalAgeDays: number;
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  previousHydatidiformMole: string;
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  familyPreviousHydatidiformMole: string;
  @ApiProperty()
  @MustSync((_, x) => isString(x), NotNull, { message: '' })
  contraceptiveDrugUsage: string;
  @ApiProperty()
  @MustSync((_, x) => isString(x), NotNull, { message: '' })
  eventsLeadingToDiagnosis: string;
  @ApiProperty()
  @MustSync((_, x) => isString(x), NotNull, { message: '' })
  diagnosisCode: string;
  @ApiProperty()
  @MustSync(
    (_, x) => {
      if (!x) {
        return true;
      }
      return isDateString(x);
    },
    NotNull,
    { message: '' },
  )
  diagnosisDate: Date;
  @ApiProperty()
  @MustSync((_, x) => isString(x), NotNull, { message: '' })
  comorbidity: string;
  @ApiProperty()
  @MustSync((_, x) => isInt(x), NotNull, { message: '' })
  figoStaging: number;
  @ApiProperty()
  @MustSync((_, x) => isInt(x), NotNull, { message: '' })
  figoScoring: number;
  @ApiProperty()
  @MustSync((_, x) => isString(x), NotNull, { message: '' })
  numberOfMetastasis: string;
  @ApiProperty()
  @MustSync((_, x) => isString(x), NotNull, { message: '' })
  metastasisLocation: string;
  @ApiProperty()
  @MustSync((_, x) => isString(x), NotNull, { message: '' })
  maximumTumorSize: string;
  @ApiProperty()
  @MustSync((_, x) => isString(x), NotNull, { message: '' })
  methodOfEvacuation: string;
  @ApiProperty()
  @MustSync(
    (_, x) => {
      if (!x) {
        return true;
      }
      return isDateString(x);
    },
    NotNull,
    { message: '' },
  )
  dateOfEvacuation: Date;
  @ApiProperty()
  @MustSync((_, x) => isString(x), NotNull, { message: '' })
  classificationOfMole: string;
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  eventsLeadingToDiagnosis1: string;
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  bhcgLevelAtDiagnosis: string;
  @ApiProperty()
  @MustSync((_, x) => isInt(x), NotNull, { message: '' })
  gestationalAge: number;
  @ApiProperty()
  @MustSync((_, x) => isInt(x), NotNull, { message: '' })
  uterineSize: number;
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  @IsDateString({ message: '' })
  visitDate: Date;
  @ApiProperty()
  @MustSync((moduleRef, property) => isString(property), NotNull, { message: '' })
  howLongDrugUsed: string;
  @ApiProperty()
  @MustSync((moduleRef, property) => isString(property), NotNull, { message: '' })
  nameOfDrug: string;
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  visitNumber: string;
  @ApiProperty()
  @IsArray()
  @ArrayMinSize(0)
  @ArrayUnique()
  @Must(
    async (moduleRef, property) => {
      const repos = await moduleRef.resolve(AppRepositoryService);
      return (await repos.drugs.createQueryBuilder().whereInIds(property).getCount()) === property.length;
    },
    NotNullAsync,
    { message: 'Invalid drug id' },
  )
  drugIds: number[];
  @ApiProperty()
  week1: number;
  @ApiProperty()
  week2: number;
  @ApiProperty()
  week3: number;
  @ApiProperty()
  week4: number;
  @ApiProperty()
  month2: number;
  @ApiProperty()
  month6: number;
  @ApiProperty()
  month12: number;
  @ApiProperty()
  cotroceptiveMethodDuringFollowUp: string;
  @ApiProperty()
  hysterectomy: boolean;
  @ApiProperty()
  chemotrophy: boolean;
}
