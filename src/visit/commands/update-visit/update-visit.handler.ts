import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { UpdateVisitCommand } from './update-visit.command';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { GetVisitDto } from '../../queries/get-visit/get-visit.dto';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { BadRequest, NotFound } from '../../../common/std-response/std-reponse.factory';
import { StdResponse } from '../../../common/std-response/std-response';
import { Validator } from '../../../common/validations/validator';
import { GetVisitHandler } from '../../queries/get-visit/get-visit.handler';
import { GetVisitQuery } from '../../queries/get-visit/get-visit.query';
import { VisitDrug } from '../../../visit-drug/visit-drug.entity';

@CommandHandler(UpdateVisitCommand)
export class UpdateVisitHandler
  extends AbstractHandler
  implements ICommandHandler<UpdateVisitCommand, StdResponse<GetVisitDto>>
{
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(command: UpdateVisitCommand): Promise<StdResponse<GetVisitDto>> {
    const visit = await this.repositoryService.visits.findOne(command.id);
    if (!visit) {
      return NotFound();
    }

    const validationResult = await Validator.validate(command);
    if (validationResult.failed()) {
      return BadRequest<any>(validationResult.getMessage());
    }

    Object.assign(visit, {
      patientId: command.patientId,
      doctorId: command.doctorId,
      gravida: command.gravida,
      para: command.para,
      twin: command.twin,
      numberOfLiveBirth: command.numberOfLiveBirth,
      abortion: command.abortion,
      causeOfAbortion: command.causeOfAbortion,
      lmp: command.lmp,
      gestationalAgeWeeks: command.gestationalAgeWeeks,
      gestationalAgeDays: command.gestationalAgeDays,
      previousHydatidiformMole: command.previousHydatidiformMole.toLowerCase(),
      familyPreviousHydatidiformMole: command.familyPreviousHydatidiformMole.toLowerCase(),
      contraceptiveDrugUsage: command.contraceptiveDrugUsage,
      eventsLeadingToDiagnosis: command.eventsLeadingToDiagnosis,
      diagnosisCode: command.diagnosisCode,
      diagnosisDate: command.diagnosisDate,
      comorbidity: command.comorbidity,
      figoStaging: command.figoStaging,
      figoScoring: command.figoScoring,
      numberOfMetastasis: command.numberOfMetastasis,
      metastasisLocation: command.metastasisLocation,
      maximumTumorSize: command.maximumTumorSize,
      methodOfEvacuation: command.methodOfEvacuation,
      dateOfEvacuation: command.dateOfEvacuation,
      classificationOfMole: command.classificationOfMole,
      eventsLeadingToDiagnosis1: command.eventsLeadingToDiagnosis1,
      bhcgLevelAtDiagnosis: command.bhcgLevelAtDiagnosis,
      gestationalAge: command.gestationalAge,
      uterineSize: command.uterineSize,
      visitDate: command.visitDate,
      visitNumber: command.visitNumber,
      nameOfDrug: command.nameOfDrug,
      howLongDrugUsed: command.howLongDrugUsed,
      week1: command.week1,
      week2: command.week2,
      week3: command.week3,
      week4: command.week4,
      month2: command.month2,
      month6: command.month6,
      month12: command.month12,
      cotroceptiveMethodDuringFollowUp: command.cotroceptiveMethodDuringFollowUp,
      hysterectomy: command.hysterectomy,
      chemotrophy: command.chemotrophy,
    });

    await this.repositoryService.visits.save(visit);

    await this.repositoryService.visitDrugs
      .createQueryBuilder()
      .delete()
      .where('visit_id = :id', { id: visit.id })
      .execute();

    const visitDrugs = command.drugIds.map((x) => {
      const visitDrug = new VisitDrug();

      visitDrug.drugId = x;
      visitDrug.visitId = visit.id;

      return visitDrug;
    });

    await this.repositoryService.visitDrugs.save(visitDrugs);

    const getVisitHandler = new GetVisitHandler(this.repositoryService, this.moduleRef);
    return await getVisitHandler.execute(new GetVisitQuery(visit.id));
  }
}
