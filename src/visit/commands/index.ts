import { CreateVisitHandler } from './create-visit/create-visit.handler';
import { UpdateVisitHandler } from './update-visit/update-visit.handler';
import { DeleteVisitHandler } from './delete-visit/delete-visit.handler';

export const CommandHandlers = [CreateVisitHandler, UpdateVisitHandler, DeleteVisitHandler];
