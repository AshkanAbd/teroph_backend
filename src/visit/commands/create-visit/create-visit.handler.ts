import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { BadRequest } from '../../../common/std-response/std-reponse.factory';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { CreateVisitCommand } from './create-visit.command';
import { Validator } from '../../../common/validations/validator';
import { GetVisitDto } from '../../queries/get-visit/get-visit.dto';
import { GetVisitQuery } from '../../queries/get-visit/get-visit.query';
import { GetVisitHandler } from '../../queries/get-visit/get-visit.handler';
import { VisitDrug } from '../../../visit-drug/visit-drug.entity';

@CommandHandler(CreateVisitCommand)
export class CreateVisitHandler
  extends AbstractHandler
  implements ICommandHandler<CreateVisitCommand, StdResponse<GetVisitDto>>
{
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(command: CreateVisitCommand): Promise<StdResponse<GetVisitDto>> {
    const validationResult = await Validator.validate(command);
    if (validationResult.failed()) {
      return BadRequest<any>(validationResult.getMessage());
    }

    let visit = this.repositoryService.visits.create({
      patientId: command.patientId,
      doctorId: command.doctorId,
      gravida: command.gravida,
      para: command.para,
      twin: command.twin,
      numberOfLiveBirth: command.numberOfLiveBirth,
      abortion: command.abortion,
      causeOfAbortion: command.causeOfAbortion,
      lmp: command.lmp,
      gestationalAgeWeeks: command.gestationalAgeWeeks,
      gestationalAgeDays: command.gestationalAgeDays,
      previousHydatidiformMole: command.previousHydatidiformMole.toLowerCase(),
      familyPreviousHydatidiformMole: command.familyPreviousHydatidiformMole.toLowerCase(),
      contraceptiveDrugUsage: command.contraceptiveDrugUsage,
      eventsLeadingToDiagnosis: command.eventsLeadingToDiagnosis,
      diagnosisCode: command.diagnosisCode,
      diagnosisDate: command.diagnosisDate,
      comorbidity: command.comorbidity,
      figoStaging: command.figoStaging,
      figoScoring: command.figoScoring,
      numberOfMetastasis: command.numberOfMetastasis,
      metastasisLocation: command.metastasisLocation,
      maximumTumorSize: command.maximumTumorSize,
      methodOfEvacuation: command.methodOfEvacuation,
      dateOfEvacuation: command.dateOfEvacuation,
      classificationOfMole: command.classificationOfMole,
      eventsLeadingToDiagnosis1: command.eventsLeadingToDiagnosis1,
      bhcgLevelAtDiagnosis: command.bhcgLevelAtDiagnosis,
      gestationalAge: command.gestationalAge,
      uterineSize: command.uterineSize,
      visitDate: command.visitDate,
      visitNumber: command.visitNumber,
      nameOfDrug: command.nameOfDrug,
      howLongDrugUsed: command.howLongDrugUsed,
      week1: command.week1,
      week2: command.week2,
      week3: command.week3,
      week4: command.week4,
      month2: command.month2,
      month6: command.month6,
      month12: command.month12,
      cotroceptiveMethodDuringFollowUp: command.cotroceptiveMethodDuringFollowUp,
      hysterectomy: command.hysterectomy,
      chemotrophy: command.chemotrophy,
    });
    visit = await this.repositoryService.visits.save(visit);

    const visitDrugs = command.drugIds.map((x) => {
      const visitDrug = new VisitDrug();

      visitDrug.drugId = x;
      visitDrug.visitId = visit.id;

      return visitDrug;
    });

    await this.repositoryService.visitDrugs.save(visitDrugs);

    const getVisitHandler = new GetVisitHandler(this.repositoryService, this.moduleRef);
    return await getVisitHandler.execute(new GetVisitQuery(visit.id));
  }
}
