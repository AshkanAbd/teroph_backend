import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  RelationId,
  UpdateDateColumn,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { Patient } from '../patient/patient.entity';
import { Doctor } from '../doctor/doctor.entity';
import { VisitDrug } from '../visit-drug/visit-drug.entity';

@Entity('visits')
export class Visit {
  @PrimaryGeneratedColumn('increment', { type: 'bigint', name: 'id' })
  id: number;
  @RelationId((object: Visit) => object.patient)
  @Column({ type: 'bigint', name: 'patient_id' })
  patientId: number;
  @RelationId((object: Visit) => object.doctor)
  @Column({ type: 'bigint', name: 'doctor_id' })
  doctorId: number;
  @Column({ type: 'smallint', name: 'gravida' })
  gravida: number;
  @Column({ type: 'smallint', name: 'para' })
  para: number;
  @Column({ type: 'boolean', name: 'twin' })
  twin: boolean;
  @Column({ type: 'varchar', name: 'number_of_live_birth', length: 20 })
  numberOfLiveBirth: string;
  @Column({ type: 'varchar', name: 'abortion', length: 20 })
  abortion: string;
  @Column({ type: 'text', name: 'cause_of_abortion' })
  causeOfAbortion: string;
  @Column({ type: 'timestamp', name: 'lmp' })
  lmp: Date;
  @Column({ type: 'smallint', name: 'gestational_age_weeks' })
  gestationalAgeWeeks: number;
  @Column({ type: 'smallint', name: 'gestational_age_days' })
  gestationalAgeDays: number;
  @Column({ type: 'boolean', name: 'previous_hydatidiform_mole' })
  previousHydatidiformMole: string;
  @Column({ type: 'boolean', name: 'family_previous_hydatidiform_mole' })
  familyPreviousHydatidiformMole: string;
  @Column({ type: 'text', name: 'contraceptive_drug_usage' })
  contraceptiveDrugUsage: string;
  @Column({ type: 'text', name: 'events_leading_to_diagnosis' })
  eventsLeadingToDiagnosis: string;
  @Column({ type: 'text', name: 'diagnosis_code' })
  diagnosisCode: string;
  @Column({ type: 'timestamp', name: 'diagnosis_date', nullable: true })
  diagnosisDate: Date;
  @Column({ type: 'text', name: 'comorbidity' })
  comorbidity: string;
  @Column({ type: 'smallint', name: 'figo_staging' })
  figoStaging: number;
  @Column({ type: 'smallint', name: 'figo_scoring' })
  figoScoring: number;
  @Column({ type: 'varchar', name: 'number_of_metastasis', length: 100 })
  numberOfMetastasis: string;
  @Column({ type: 'varchar', name: 'metastasis_location', length: 100 })
  metastasisLocation: string;
  @Column({ type: 'varchar', name: 'maximum_tumor_size', length: 20 })
  maximumTumorSize: string;
  @Column({ type: 'varchar', name: 'method_of_evacuation', length: 30 })
  methodOfEvacuation: string;
  @Column({ type: 'timestamp', name: 'date_of_evacuation' })
  dateOfEvacuation: Date;
  @Column({ type: 'varchar', name: 'classification_of_mole', length: 30 })
  classificationOfMole: string;
  @Column({ type: 'text', name: 'events_leading_to_diagnosis1' })
  eventsLeadingToDiagnosis1: string;
  @Column({ type: 'text', name: 'bhcg_level_at_diagnosis' })
  bhcgLevelAtDiagnosis: string;
  @Column({ type: 'smallint', name: 'gestational_age' })
  gestationalAge: number;
  @Column({ type: 'int', name: 'uterine_size' })
  uterineSize: number;
  @Column({ type: 'timestamp', name: 'visit_date', nullable: true })
  visitDate: Date;
  @Column({ type: 'text', name: 'visit_number', nullable: true })
  visitNumber: string;
  @Column({ type: 'text', name: 'name_of_drug', nullable: true })
  nameOfDrug: string;
  @Column({ type: 'text', name: 'how_long_drug_used', nullable: true })
  howLongDrugUsed: string;
  @Column({ type: 'int', name: 'week_1', nullable: true })
  week1: number;
  @Column({ type: 'int', name: 'week_2', nullable: true })
  week2: number;
  @Column({ type: 'int', name: 'week_3', nullable: true })
  week3: number;
  @Column({ type: 'int', name: 'week_4', nullable: true })
  week4: number;
  @Column({ type: 'int', name: 'month_2', nullable: true })
  month2: number;
  @Column({ type: 'int', name: 'month_6', nullable: true })
  month6: number;
  @Column({ type: 'int', name: 'month_12', nullable: true })
  month12: number;
  @Column({ type: 'text', name: 'cotroceptive_method_during_follow_up', nullable: true })
  cotroceptiveMethodDuringFollowUp: string;
  @Column({ type: 'boolean', name: 'hysterectomy', nullable: true })
  hysterectomy: boolean;
  @Column({ type: 'boolean', name: 'chemotrophy', nullable: true })
  chemotrophy: boolean;
  @CreateDateColumn({ type: 'timestamp', name: 'created_at', default: 'now()', nullable: false })
  createdAt: Date;
  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at', default: 'now()', nullable: false })
  updatedAt: Date;
  @DeleteDateColumn({ type: 'timestamp', name: 'deleted_at', nullable: true })
  deletedAt: Date;
  @ManyToOne(() => Patient, (patient) => patient.visits)
  @JoinColumn({ name: 'patient_id' })
  patient: Patient;
  @ManyToOne(() => Doctor, (doctor) => doctor.visits)
  @JoinColumn({ name: 'doctor_id' })
  doctor: Doctor;
  @OneToMany(() => Visit, (visit) => visit.doctor, {
    cascade: true,
    nullable: false,
    onDelete: 'CASCADE',
  })
  visitDrug: VisitDrug[];
}
