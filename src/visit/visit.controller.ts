import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Res,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { AbstractController } from '../common/extensions/abstract-controller';
import { ApiBearerAuth, ApiBody, ApiExtraModels, ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import { AdminAuthGuard } from '../auth/admin/admin-auth.guard';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateVisitCommand } from './commands/create-visit/create-visit.command';
import { Response } from 'express';
import { TransformerPipe } from '../common/pipes/transformer.pipe';
import { NotFoundStdException } from '../common/std-response/std-reponse.factory';
import { GetVisitQuery } from './queries/get-visit/get-visit.query';
import { GetVisitDto } from './queries/get-visit/get-visit.dto';
import { ApiStdResponse } from '../common/swagger/ApiStdResponse';
import { ApiPaginationQuery } from '../common/swagger/ApiPaginationQuery';
import { ApiPaginationResponse } from '../common/swagger/ApiPaginationResponse';
import { GetVisitListDto } from './queries/get-visit-list/get-visit-list.dto';
import { GetVisitListQuery } from './queries/get-visit-list/get-visit-list.query';
import { UpdateVisitCommand } from './commands/update-visit/update-visit.command';
import { DeleteVisitCommand } from './commands/delete-visit/delete-visit.command';

@ApiBearerAuth()
@ApiTags('visit')
@Controller('visit')
@UseGuards(AdminAuthGuard)
export class VisitController extends AbstractController {
  constructor(private readonly commandBus: CommandBus, private readonly queryBus: QueryBus) {
    super();
  }

  @Get()
  @ApiPaginationQuery()
  @ApiQuery({ name: 'doctorId', type: Number, required: false })
  @ApiQuery({ name: 'patientId', type: Number, required: false })
  @ApiPaginationResponse(GetVisitListDto)
  @ApiExtraModels(GetVisitListDto)
  async getVisitList(
    @Res() res: Response,
    @Query('doctorId') doctorId: number,
    @Query('patientId') patientId: number,
    @Query('page') page: number,
    @Query('pageSize') pageSize: number,
  ) {
    this.base(res, await this.queryBus.execute(new GetVisitListQuery(patientId, doctorId, page, pageSize)));
  }

  @Post()
  @ApiBody({
    type: CreateVisitCommand,
  })
  @ApiStdResponse(GetVisitDto)
  @UsePipes(new TransformerPipe('body'))
  async createVisit(@Body() createVisitCommand: CreateVisitCommand, @Res() res: Response) {
    this.base(res, await this.commandBus.execute(createVisitCommand));
  }

  @Get(':id')
  @ApiParam({ name: 'id', type: Number, required: true })
  @ApiStdResponse(GetVisitDto)
  @ApiExtraModels(GetVisitDto)
  async getVisit(
    @Param('id', new ParseIntPipe({ exceptionFactory: NotFoundStdException })) id: number,
    @Res() res: Response,
  ) {
    this.base(res, await this.queryBus.execute(new GetVisitQuery(id)));
  }

  @Put(':id')
  @ApiBody({
    type: UpdateVisitCommand,
  })
  @ApiStdResponse(GetVisitDto)
  @UsePipes(new TransformerPipe('body'))
  async updateVisit(
    @Param('id', new ParseIntPipe({ exceptionFactory: NotFoundStdException })) id: number,
    @Body() updateVisitCommand: UpdateVisitCommand,
    @Res() res: Response,
  ) {
    updateVisitCommand.id = id;
    this.base(res, await this.commandBus.execute(updateVisitCommand));
  }

  @Delete(':id')
  @ApiPaginationResponse(GetVisitDto)
  async deleteVisit(
    @Param('id', new ParseIntPipe({ exceptionFactory: NotFoundStdException })) id: number,
    @Res() res: Response,
  ) {
    this.base(res, await this.commandBus.execute(new DeleteVisitCommand(id)));
  }
}
