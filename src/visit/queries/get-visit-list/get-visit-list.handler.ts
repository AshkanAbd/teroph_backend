import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetVisitListQuery } from './get-visit-list.query';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { PaginationDto } from '../../../common/pagination/pagination.dto';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { BadRequest, Ok } from '../../../common/std-response/std-reponse.factory';
import { Validator } from '../../../common/validations/validator';
import { GetVisitListDto } from './get-visit-list.dto';
import { GetVisitDoctorDto, GetVisitPatientDto } from '../get-visit/get-visit.dto';

@QueryHandler(GetVisitListQuery)
export class GetVisitListHandler
  extends AbstractHandler
  implements IQueryHandler<GetVisitListQuery, StdResponse<PaginationDto<GetVisitListDto>>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(query: GetVisitListQuery): Promise<StdResponse<PaginationDto<GetVisitListDto>>> {
    const validationResult = await Validator.validate(query);
    if (validationResult.failed()) {
      return BadRequest<any>(validationResult.getMessage());
    }

    let queryBuilder = this.repositoryService.visits
      .createQueryBuilder('visits')
      // todo uncomment below line
      // .orderBy('visits.created_at', 'DESC')
      .innerJoinAndSelect('visits.doctor', 'doctor')
      .innerJoinAndSelect('visits.patient', 'patient');

    if (query.doctorId != null) {
      queryBuilder = queryBuilder.where('visits.doctor_id = :doctorId', { doctorId: query.doctorId });
    }

    if (query.patientId != null) {
      queryBuilder = queryBuilder.where('visits.patient_id = :patientId', { patientId: query.patientId });
    }

    const visits = await queryBuilder.pagination({
      pageSize: query.pageSize,
      page: query.page,
    });

    const getVisitListDto = new PaginationDto<GetVisitListDto>({
      pageCount: visits.pageCount,
      currentPage: visits.currentPage,
      currentPageSize: visits.currentPageSize,
      total: visits.total,
    });

    getVisitListDto.list = visits.list.map((x) => {
      const visitDto = new GetVisitListDto();

      visitDto.id = x.id.toString();
      visitDto.createdAt = x.createdAt;
      visitDto.visitNumber = x.visitNumber;
      visitDto.visitDate = x.visitDate;

      visitDto.patient = new GetVisitPatientDto();
      visitDto.patient.id = x.patient.id.toString();
      visitDto.patient.fullName = `${x.patient.firstname} ${x.patient.lastname}`;
      visitDto.patient.nationalId = x.patient.nationalId;
      visitDto.patient.recordNumber = x.patient.recordNumber;

      visitDto.doctor = new GetVisitDoctorDto();
      visitDto.doctor.id = x.doctor.id.toString();
      visitDto.doctor.fullName = x.doctor.fullName;
      visitDto.doctor.medicalNumber = x.doctor.medicalNumber;

      return visitDto;
    });

    // todo comment below line.
    getVisitListDto.list = getVisitListDto.list.sort((x, y) => y.createdAt.getTime() - x.createdAt.getTime());

    return Ok(getVisitListDto);
  }
}
