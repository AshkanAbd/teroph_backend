import { isInt, IsInt } from 'class-validator';
import { Must, NotNullAsync } from '../../../common/validations/custom-validation/must.rule';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { MustSync, NotNull } from '../../../common/validations/custom-validation/must-sync.rule';

export class GetVisitListQuery {
  @MustSync((_, x) => isInt(parseInt(x)), NotNull, { message: 'PatientId must be integer.' })
  @Must(
    async (moduleRef, property) => {
      const repository = await moduleRef.resolve(AppRepositoryService);
      return !!(await repository.patients.findOne(property));
    },
    NotNullAsync,
    { message: 'Invalid patientId.' },
  )
  patientId: number;
  @MustSync((_, x) => isInt(parseInt(x)), NotNull, { message: 'DoctorId must be integer.' })
  @Must(
    async (moduleRef, property) => {
      const repository = await moduleRef.resolve(AppRepositoryService);
      return !!(await repository.doctors.findOne(property));
    },
    NotNullAsync,
    { message: 'Invalid doctorId.' },
  )
  doctorId: number;
  page: number;
  pageSize: number;

  constructor(patientId: number, doctorId: number, page: number, pageSize: number) {
    this.patientId = patientId;
    this.doctorId = doctorId;
    this.page = page;
    this.pageSize = pageSize;
  }
}
