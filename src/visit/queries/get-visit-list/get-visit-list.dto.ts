import { GetVisitDoctorDto, GetVisitPatientDto } from '../get-visit/get-visit.dto';
import { ApiProperty } from '@nestjs/swagger';

export class GetVisitListDto {
  @ApiProperty()
  id: string;
  @ApiProperty()
  doctor: GetVisitDoctorDto;
  @ApiProperty()
  patient: GetVisitPatientDto;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  visitNumber: string;
  @ApiProperty()
  visitDate: Date;
}
