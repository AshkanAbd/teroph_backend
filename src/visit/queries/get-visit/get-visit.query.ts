export class GetVisitQuery {
  id: number;

  constructor(id: number) {
    this.id = id;
  }
}
