import { ApiProperty } from '@nestjs/swagger';

export class GetVisitPatientDto {
  @ApiProperty()
  id: string;
  @ApiProperty()
  fullName: string;
  @ApiProperty()
  nationalId: string;
  @ApiProperty()
  recordNumber: string;
}

export class GetVisitDoctorDto {
  @ApiProperty()
  id: string;
  @ApiProperty()
  fullName: string;
  @ApiProperty()
  medicalNumber: string;
}

export class GetVisitDrugDto {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
}

export class GetVisitDto {
  @ApiProperty()
  id: string;
  @ApiProperty()
  gravida: number;
  @ApiProperty()
  para: number;
  @ApiProperty()
  twin: boolean;
  @ApiProperty()
  numberOfLiveBirth: string;
  @ApiProperty()
  abortion: string;
  @ApiProperty()
  causeOfAbortion: string;
  @ApiProperty()
  lmp: Date;
  @ApiProperty()
  gestationalAgeWeeks: number;
  @ApiProperty()
  gestationalAgeDays: number;
  @ApiProperty()
  previousHydatidiformMole: string;
  @ApiProperty()
  familyPreviousHydatidiformMole: string;
  @ApiProperty()
  contraceptiveDrugUsage: string;
  @ApiProperty()
  eventsLeadingToDiagnosis: string;
  @ApiProperty()
  diagnosisCode: string;
  @ApiProperty()
  diagnosisDate: Date;
  @ApiProperty()
  comorbidity: string;
  @ApiProperty()
  figoStaging: number;
  @ApiProperty()
  figoScoring: number;
  @ApiProperty()
  numberOfMetastasis: string;
  @ApiProperty()
  metastasisLocation: string;
  @ApiProperty()
  maximumTumorSize: string;
  @ApiProperty()
  methodOfEvacuation: string;
  @ApiProperty()
  dateOfEvacuation: Date;
  @ApiProperty()
  classificationOfMole: string;
  @ApiProperty()
  eventsLeadingToDiagnosis1: string;
  @ApiProperty()
  bhcgLevelAtDiagnosis: string;
  @ApiProperty()
  gestationalAge: number;
  @ApiProperty()
  uterineSize: number;
  @ApiProperty()
  visitNumber: string;
  @ApiProperty()
  nameOfDrug: string;
  @ApiProperty()
  howLongDrugUsed: string;
  @ApiProperty()
  visitDate: Date;
  @ApiProperty()
  week1: number;
  @ApiProperty()
  week2: number;
  @ApiProperty()
  week3: number;
  @ApiProperty()
  week4: number;
  @ApiProperty()
  month2: number;
  @ApiProperty()
  month6: number;
  @ApiProperty()
  month12: number;
  @ApiProperty()
  cotroceptiveMethodDuringFollowUp: string;
  @ApiProperty()
  hysterectomy: boolean;
  @ApiProperty()
  chemotrophy: boolean;
  @ApiProperty()
  patient: GetVisitPatientDto;
  @ApiProperty()
  doctor: GetVisitDoctorDto;
  @ApiProperty()
  drugs: GetVisitDrugDto[];
}
