import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetVisitQuery } from './get-visit.query';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { NotFound, Ok } from '../../../common/std-response/std-reponse.factory';
import { GetVisitDoctorDto, GetVisitDrugDto, GetVisitDto, GetVisitPatientDto } from './get-visit.dto';
import { Drug } from '../../../drug/drug.entity';
import { VisitDrug } from '../../../visit-drug/visit-drug.entity';
import { ApiProperty } from '@nestjs/swagger';

@QueryHandler(GetVisitQuery)
export class GetVisitHandler extends AbstractHandler implements IQueryHandler<GetVisitQuery, StdResponse<GetVisitDto>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(query: GetVisitQuery): Promise<StdResponse<GetVisitDto>> {
    const visit = await this.repositoryService.visits.findOne(query.id, {
      relations: ['patient', 'doctor'],
    });

    if (!visit) {
      return NotFound();
    }

    const getVisitDto = new GetVisitDto();

    getVisitDto.id = visit.id.toString();
    getVisitDto.gravida = visit.gravida;
    getVisitDto.para = visit.para;
    getVisitDto.twin = visit.twin;
    getVisitDto.numberOfLiveBirth = visit.numberOfLiveBirth;
    getVisitDto.abortion = visit.abortion;
    getVisitDto.causeOfAbortion = visit.causeOfAbortion;
    getVisitDto.lmp = visit.lmp;
    getVisitDto.gestationalAgeWeeks = visit.gestationalAgeWeeks;
    getVisitDto.gestationalAgeDays = visit.gestationalAgeDays;
    getVisitDto.previousHydatidiformMole = visit.previousHydatidiformMole;
    getVisitDto.familyPreviousHydatidiformMole = visit.familyPreviousHydatidiformMole;
    getVisitDto.contraceptiveDrugUsage = visit.contraceptiveDrugUsage;
    getVisitDto.eventsLeadingToDiagnosis = visit.eventsLeadingToDiagnosis;
    getVisitDto.diagnosisCode = visit.diagnosisCode;
    getVisitDto.diagnosisDate = visit.diagnosisDate;
    getVisitDto.comorbidity = visit.comorbidity;
    getVisitDto.figoStaging = visit.figoStaging;
    getVisitDto.figoScoring = visit.figoScoring;
    getVisitDto.numberOfMetastasis = visit.numberOfMetastasis;
    getVisitDto.metastasisLocation = visit.metastasisLocation;
    getVisitDto.maximumTumorSize = visit.maximumTumorSize;
    getVisitDto.methodOfEvacuation = visit.methodOfEvacuation;
    getVisitDto.dateOfEvacuation = visit.dateOfEvacuation;
    getVisitDto.classificationOfMole = visit.classificationOfMole;
    getVisitDto.eventsLeadingToDiagnosis1 = visit.eventsLeadingToDiagnosis1;
    getVisitDto.bhcgLevelAtDiagnosis = visit.bhcgLevelAtDiagnosis;
    getVisitDto.gestationalAge = visit.gestationalAge;
    getVisitDto.uterineSize = visit.uterineSize;
    getVisitDto.visitNumber = visit.visitNumber;
    getVisitDto.nameOfDrug = visit.nameOfDrug;
    getVisitDto.week1 = visit.week1;
    getVisitDto.week2 = visit.week2;
    getVisitDto.week3 = visit.week3;
    getVisitDto.week4 = visit.week4;
    getVisitDto.month2 = visit.month2;
    getVisitDto.month6 = visit.month6;
    getVisitDto.month12 = visit.month12;
    getVisitDto.cotroceptiveMethodDuringFollowUp = visit.cotroceptiveMethodDuringFollowUp;
    getVisitDto.hysterectomy = visit.hysterectomy;
    getVisitDto.chemotrophy = visit.chemotrophy;
    getVisitDto.howLongDrugUsed = visit.howLongDrugUsed;
    getVisitDto.visitDate = visit.visitDate;
    getVisitDto.patient = new GetVisitPatientDto();
    getVisitDto.patient.id = visit.patient.id.toString();
    getVisitDto.patient.fullName = `${visit.patient.firstname} ${visit.patient.lastname}`;
    getVisitDto.patient.nationalId = visit.patient.nationalId;
    getVisitDto.patient.recordNumber = visit.patient.recordNumber;

    getVisitDto.doctor = new GetVisitDoctorDto();
    getVisitDto.doctor.id = visit.doctor.id.toString();
    getVisitDto.doctor.fullName = visit.doctor.fullName;
    getVisitDto.doctor.medicalNumber = visit.doctor.medicalNumber;

    const drugs = await this.repositoryService.visitDrugs
      .createQueryBuilder('visit_drugs')
      .innerJoinAndSelect('visit_drugs.drug', 'drugs')
      .where('visit_drugs.visit_id = :id', { id: visit.id })
      .getMany();

    getVisitDto.drugs = drugs.map((x: VisitDrug) => {
      const drug = new GetVisitDrugDto();

      drug.id = x.drug.id.toString();
      drug.name = x.drug.name;

      return drug;
    });

    return Ok(getVisitDto);
  }
}
