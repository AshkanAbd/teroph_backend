import { GetVisitHandler } from './get-visit/get-visit.handler';
import { GetVisitListHandler } from './get-visit-list/get-visit-list.handler';

export const QueryHandlers = [GetVisitHandler, GetVisitListHandler];
