import { Patient } from './patient/patient.entity';
import { Doctor } from './doctor/doctor.entity';
import { Drug } from './drug/drug.entity';
import { Admin } from './admin/admin.entity';
import { Visit } from './visit/visit.entity';
import { VisitDrug } from './visit-drug/visit-drug.entity';
import { State } from './state/state.entity';
import { City } from './city/city.entity';

export const Entities = [Patient, Doctor, Drug, Admin, Visit, VisitDrug, State, City];
