import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PatientModule } from './patient/patient.module';
import { AppRepositoryService } from './services/app-repository.service';
import { MustRule } from './common/validations/custom-validation/must.rule';
import { MustSyncRule } from './common/validations/custom-validation/must-sync.rule';
import { DoctorModule } from './doctor/doctor.module';
import { Entities } from './app.entities';
import { DrugModule } from './drug/drug.module';
import { AdminModule } from './admin/admin.module';
import './common/pagination/pagination';
import { AdminAuthService } from './auth/admin/admin-auth.service';
import { VisitModule } from './visit/visit.module';
import { VisitDrugModule } from './visit-drug/visit-drug.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { ReportModule } from './report/report.module';
import { CityModule } from './city/city.module';
import { StateModule } from './state/state.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    TypeOrmModule.forFeature([...Entities]),
    PatientModule,
    DoctorModule,
    DrugModule,
    AdminModule,
    VisitModule,
    VisitDrugModule,
    DashboardModule,
    ReportModule,
    CityModule,
    StateModule,
  ],
  controllers: [],
  providers: [AppRepositoryService, MustRule, MustSyncRule, AdminAuthService],
  exports: [],
})
export class AppModule {
  //
}
