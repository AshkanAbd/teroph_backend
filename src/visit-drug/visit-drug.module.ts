import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Entities } from '../app.entities';

@Module({
  imports: [TypeOrmModule.forFeature([...Entities])],
  controllers: [],
})
export class VisitDrugModule {
}
