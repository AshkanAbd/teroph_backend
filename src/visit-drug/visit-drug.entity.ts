import {
  Column,
  CreateDateColumn, DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  RelationId,
  UpdateDateColumn
} from 'typeorm';
import { Drug } from '../drug/drug.entity';
import { Visit } from '../visit/visit.entity';

@Entity('visit_drugs')
export class VisitDrug {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'id' })
  public id: number;
  @RelationId((visitDrug: VisitDrug) => visitDrug.drug)
  @Column({ type: 'bigint', name: 'drug_id' })
  drugId: number;
  @RelationId((visitDrug: VisitDrug) => visitDrug.visit)
  @Column({ type: 'bigint', name: 'visit_id' })
  visitId: number;
  @CreateDateColumn({ type: 'timestamp', name: 'created_at', nullable: false })
  public createdAt: Date;
  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at', nullable: false })
  public updatedAt: Date;
  @DeleteDateColumn({ type: 'timestamp', name: 'deleted_at', nullable: true })
  public deletedAt: Date;
  @ManyToOne(() => Drug, (drug: Drug) => drug.visitDrug)
  @JoinColumn({ name: 'drug_id' })
  drug: Drug;
  @ManyToOne(() => Visit, (visit: Visit) => visit.visitDrug)
  @JoinColumn({ name: 'visit_id' })
  visit: Visit;
}
