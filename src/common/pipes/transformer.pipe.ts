import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class TransformerPipe implements PipeTransform {
  private types: ('body' | 'query' | 'param' | 'custom')[] = [];

  constructor(...types: ('body' | 'query' | 'param' | 'custom')[]) {
    this.types = types;
  }

  transform(value: any, metadata: ArgumentMetadata) {
    if (!this.types.includes(metadata.type)) {
      return value;
    }
    return Object.assign(new metadata.metatype(), value);
  }
}
