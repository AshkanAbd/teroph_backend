import { ApiProperty } from '@nestjs/swagger';

export class ValidationFailure {
  @ApiProperty()
  public property: string;
  @ApiProperty()
  public message: string;

  constructor(property: string, message: string) {
    this.property = property;
    this.message = message;
  }
}
