import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { Injectable } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';

@ValidatorConstraint({ name: 'Must', async: true })
@Injectable()
export class MustRule implements ValidatorConstraintInterface {
  constructor(private moduleRef: ModuleRef) {
    //
  }

  hasWhenFunction(data: ValidationArguments): boolean {
    return !!data.constraints[1] && typeof data.constraints[1] === 'function';
  }

  async runWhenFunction(value: any, data: ValidationArguments): Promise<boolean> {
    return await data.constraints[1](this.moduleRef, data.object, value);
  }

  async validate(value: any, data: ValidationArguments) {
    try {
      if (this.hasWhenFunction(data)) {
        if (await this.runWhenFunction(value, data)) {
          return await data.constraints[0](this.moduleRef, value, data.object);
        }
        return true;
      }
      return await data.constraints[0](this.moduleRef, value, data.object);
    } catch (e) {
      return false;
    }
  }

  defaultMessage(args: ValidationArguments) {
    return `Must rule failed`;
  }
}

export function Must(
  rule: (moduleRef: ModuleRef, property: any, object: any) => Promise<boolean>,
  when: (moduleRef: ModuleRef, object: any, property: any) => Promise<boolean> = async () => true,
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      name: 'must',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [rule, when],
      options: validationOptions,
      validator: MustRule,
    });
  };
}

export async function NotNullAsync(moduleRef: ModuleRef, object: any, property: any): Promise<boolean> {
  return property !== undefined && property !== null;
}
