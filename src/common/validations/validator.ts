import { ValidatorOptions } from 'class-validator/types/validation/ValidatorOptions';
import { validate } from 'class-validator';
import { ValidationError } from 'class-validator/types/validation/ValidationError';
import { ValidationFailure } from './validation-failure';

export class Validator {
  private readonly failures: ValidationFailure[];
  private readonly errors: ValidationError[];

  constructor(errors: ValidationError[]) {
    this.errors = errors;
    this.failures = [];
    for (const failures of errors) {
      for (const failure of Object.values(failures.constraints)) {
        this.failures.push(new ValidationFailure(failures.property, failure));
      }
    }
  }

  static async validate(object: object, validatorOptions?: ValidatorOptions): Promise<Validator> {
    return new Validator(await validate(object, validatorOptions));
  }

  public failed() {
    return this.failures.length !== 0;
  }

  public getErrors() {
    return this.errors;
  }

  public getMessage() {
    return this.failures;
  }
}
