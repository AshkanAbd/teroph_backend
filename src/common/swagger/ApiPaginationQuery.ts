import { applyDecorators } from '@nestjs/common';
import { ApiQuery } from '@nestjs/swagger';
import { ApiQueryOptions } from '@nestjs/swagger/dist/decorators/api-query.decorator';

export const ApiPaginationQuery = (options: ApiQueryOptions = {}) => {
  if (!options) {
    options = {};
  }
  return applyDecorators(
    ApiQuery(Object.assign(options, { name: 'pageSize', required: false })),
    ApiQuery(Object.assign(options, { name: 'page', required: false })),
  );
};
