import { applyDecorators } from '@nestjs/common';
import { ApiQuery } from '@nestjs/swagger';
import { ApiQueryOptions } from '@nestjs/swagger/dist/decorators/api-query.decorator';

export const ApiSearchQuery = (options: ApiQueryOptions = {}) => {
  if (!options) {
    options = {};
  }
  return applyDecorators(ApiQuery(Object.assign(options, { name: 'search', required: false })));
};
