import { ApiProperty } from '@nestjs/swagger';

export class StdResponse<T> {
  @ApiProperty()
  public status: string;
  @ApiProperty()
  public message: string;
  // @ApiProperty()
  public data: T;

  constructor(data: T, message: string, status: string) {
    this.status = status;
    this.message = message;
    this.data = data;
  }
}
