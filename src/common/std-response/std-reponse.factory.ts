import { StdResponse } from './std-response';
import { StdStatus } from './std-status';
import {
  BadRequestException,
  ForbiddenException,
  HttpException,
  InternalServerErrorException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';

export function Base<T>(data: T = null, message = '', status: string): StdResponse<T> {
  return new StdResponse<T>(data, message, status);
}

export function Ok<T>(data: T = null, message = ''): StdResponse<T> {
  return Base<T>(data, message, StdStatus.status['200']);
}

export function OkMsg<T>(message = ''): StdResponse<T> {
  return Ok<T>(null, message);
}

export function BadRequest<T>(data: T = null, message = ''): StdResponse<T> {
  return Base<T>(data, message, StdStatus.status['422']);
}

export function BadRequestMsg<T>(message = ''): StdResponse<T> {
  return BadRequest<T>(null, message);
}

export function BadRequestStdException<T>(message = ''): HttpException {
  return new BadRequestException(BadRequestMsg<T>(message));
}

export function NotFound<T>(data: T = null, message = ''): StdResponse<T> {
  return Base<T>(data, message, StdStatus.status['404']);
}

export function NotFoundMsg<T>(message = ''): StdResponse<T> {
  return NotFound<T>(null, message);
}

export function NotFoundStdException<T>(message = ''): HttpException {
  return new NotFoundException(NotFoundMsg<T>(message));
}

export function NotAuth<T>(data: T = null, message = ''): StdResponse<T> {
  return Base<T>(data, message, StdStatus.status['401']);
}

export function NotAuthMsg<T>(message = ''): StdResponse<T> {
  return NotAuth<T>(null, message);
}

export function NotAuthStdException<T>(message = ''): HttpException {
  return new UnauthorizedException(NotAuthMsg<T>(message));
}

export function PermissionDenied<T>(data: T = null, message = ''): StdResponse<T> {
  return Base<T>(data, message, StdStatus.status['403']);
}

export function PermissionDeniedMsg<T>(message = ''): StdResponse<T> {
  return PermissionDenied<T>(null, message);
}

export function PermissionDeniedStdException<T>(message = ''): HttpException {
  return new ForbiddenException(PermissionDeniedMsg<T>(message));
}

export function InternalError<T>(data: T = null, message = ''): StdResponse<T> {
  return Base<T>(data, message, StdStatus.status['500']);
}

export function InternalErrorMsg<T>(message = ''): StdResponse<T> {
  return InternalError<T>(null, message);
}

export function InternalErrorStdException<T>(message = ''): HttpException {
  return new InternalServerErrorException(InternalErrorMsg<T>(message));
}
