import { Response } from 'express';
import { StdResponse } from '../std-response/std-response';
import { StdStatus } from '../std-response/std-status';

export abstract class AbstractController {
  protected base<T>(res: Response, stdRes: StdResponse<T>): void {
    res.status(StdStatus.getCode(stdRes.status)).json(stdRes);
  }
}
