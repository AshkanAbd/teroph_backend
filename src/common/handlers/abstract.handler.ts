import { AppRepositoryService } from '../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';

export abstract class AbstractHandler {
  public constructor(protected repositoryService: AppRepositoryService, protected moduleRef: ModuleRef) {
    //
  }
}
