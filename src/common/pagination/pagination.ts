import { SelectQueryBuilder } from 'typeorm';
import { PaginationDto } from './pagination.dto';
import { Request } from 'express';

declare module 'typeorm/query-builder/SelectQueryBuilder' {
  interface SelectQueryBuilder<Entity> {
    pagination(data: { req?: Request; page?: number; pageSize?: number }): Promise<PaginationDto<Entity>>;
  }
}

function readFromRequest(req: Request, key: string, defaultValue: number): number {
  if (!!req.query[key] && !!parseInt(req.query[key].toString())) {
    return parseInt(req.query[key].toString());
  }

  return defaultValue;
}

function readFromPaginationRequest(req: Request): [number, number] {
  if (!req) {
    return [undefined, undefined];
  }

  return [readFromRequest(req, 'page', 1), readFromRequest(req, 'pageSize', 10)];
}

SelectQueryBuilder.prototype.pagination = pagination;

export async function pagination<Entity>(data: {
  req?: Request;
  page?: number;
  pageSize?: number;
}): Promise<PaginationDto<Entity>> {
  const pagination = new PaginationDto<Entity>();

  let [page, pageSize] = readFromPaginationRequest(data.req);

  if (!page) {
    page = !!data.page && !!parseInt(<any>data.page) ? parseInt(<any>data.page) : 1;
  }
  if (!pageSize) {
    pageSize = !!data.pageSize && !!parseInt(<any>data.pageSize) ? parseInt(<any>data.pageSize) : 10;
  }

  pagination.total = await this.getCount();
  pagination.currentPageSize = pageSize;
  pagination.currentPage = page;

  if (pageSize !== -1) {
    pagination.list = await this.skip((page - 1) * pageSize)
      .take(pageSize)
      .getMany();
  } else {
    pagination.list = await this.getMany();
    pagination.currentPageSize = pagination.total === 0 ? 1 : pagination.total;
  }
  pagination.pageCount = Math.ceil(pagination.total / pagination.currentPageSize);

  return pagination;
}
