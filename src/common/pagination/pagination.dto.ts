import { ApiProperty } from '@nestjs/swagger';

export class PaginationDto<T> {
  constructor(
    options: {
      pageCount?: number;
      currentPage?: number;
      currentPageSize?: number;
      total?: number;
    } = {},
  ) {
    Object.assign(this, options);
  }

  @ApiProperty()
  public pageCount: number;
  @ApiProperty()
  public currentPage: number;
  @ApiProperty()
  public currentPageSize: number;
  @ApiProperty()
  public total: number;

  public list: T[];
}
