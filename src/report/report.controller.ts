import { Controller, Get, Param, ParseIntPipe, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiExtraModels, ApiParam, ApiTags } from '@nestjs/swagger';
import { AdminAuthGuard } from '../auth/admin/admin-auth.guard';
import { AbstractController } from '../common/extensions/abstract-controller';
import { QueryBus } from '@nestjs/cqrs';
import { GetUserReportDto } from './queries/get-user-report/get-user-report.dto';
import { ApiStdResponse } from '../common/swagger/ApiStdResponse';
import { Response } from 'express';
import { GetUserReportQuery } from './queries/get-user-report/get-user-report.query';
import { NotFoundStdException } from '../common/std-response/std-reponse.factory';
import { GetAgeReportDto } from './queries/get-age-report/get-age-report.dto';
import { GetAgeReportQuery } from './queries/get-age-report/get-age-report.query';
import { GetCityReportDto } from './queries/get-city-report/get-city-report.dto';
import { GetCityReportQuery } from './queries/get-city-report/get-city-report.query';

@Controller('report')
// @ApiBearerAuth()
@ApiTags('report')
// @UseGuards(AdminAuthGuard)
export class ReportController extends AbstractController {
  constructor(private queryBus: QueryBus) {
    super();
  }

  @Get('/patientReport/:id')
  @ApiParam({ name: 'id' })
  @ApiExtraModels(GetUserReportDto)
  @ApiStdResponse(GetUserReportDto)
  async getUserReport(
    @Param('id', new ParseIntPipe({ exceptionFactory: NotFoundStdException })) id: number,
    @Res() res: Response,
  ) {
    const reportQuery = new GetUserReportQuery();
    reportQuery.id = id;
    this.base(res, await this.queryBus.execute(reportQuery));
  }

  @Get('/ageReport')
  @ApiExtraModels(GetAgeReportDto)
  @ApiStdResponse(GetAgeReportDto)
  async getAgeReport(@Res() res: Response) {
    this.base(res, await this.queryBus.execute(new GetAgeReportQuery()));
  }

  @Get('/cityReport')
  @ApiExtraModels(GetCityReportDto)
  @ApiStdResponse(GetCityReportDto)
  async getCityReport(@Res() res: Response) {
    this.base(res, await this.queryBus.execute(new GetCityReportQuery()));
  }
}
