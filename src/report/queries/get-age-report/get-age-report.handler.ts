import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { GetAgeReportQuery } from './get-age-report.query';
import { StdResponse } from '../../../common/std-response/std-response';
import { GetAgeReportDto } from './get-age-report.dto';
import { ModuleRef } from '@nestjs/core';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { Ok } from '../../../common/std-response/std-reponse.factory';

@QueryHandler(GetAgeReportQuery)
export class GetAgeReportHandler
  extends AbstractHandler
  implements IQueryHandler<GetAgeReportQuery, StdResponse<GetAgeReportDto>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(query: GetAgeReportQuery): Promise<StdResponse<GetAgeReportDto>> {
    const rawReport = await this.repositoryService.patients
      .createQueryBuilder('patients')
      .select(['date_trunc(\'year\', patients.birthdate)', 'count(\'*\')'])
      .groupBy('date_trunc(\'year\', patients.birthdate)')
      .getRawMany();

    const year = new Date().getFullYear();

    const reports = rawReport.map((x) => {
      const date = new Date(x.date_trunc);

      const item = {
        count: parseInt(x.count),
        age: year - date.getFullYear(),
      };
      return item;
    });

    /*
    تفکیک مراجعات بر اساس محل سکونت مشخص کردن بیشتر مناطق درگیر بیماری از جدول دموگرافیک آیتم residense address
     */

    return Ok(reports);
  }
}
