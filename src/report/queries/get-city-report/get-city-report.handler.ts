import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetCityReportQuery } from './get-city-report.query';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { GetCityReportDto } from './get-city-report.dto';
import { Ok } from '../../../common/std-response/std-reponse.factory';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { raw } from 'express';

@QueryHandler(GetCityReportQuery)
export class GetCityReportHandler
  extends AbstractHandler
  implements IQueryHandler<GetCityReportQuery, StdResponse<GetCityReportDto>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(query: GetCityReportQuery): Promise<StdResponse<GetCityReportDto>> {
    let rawCityReport = await this.repositoryService.patients
      .createQueryBuilder('patients')
      .innerJoin('patients.city', 'city')
      .groupBy('city.name')
      .select(['count(patients.*)', 'city.name'])
      .getRawMany();

    let rawStateReport = await this.repositoryService.patients
      .createQueryBuilder('patients')
      .innerJoin('patients.city', 'city')
      .innerJoin('city.state', 'state')
      .groupBy('state.name')
      .select(['count(patients.*)', 'state.name'])
      .getRawMany();

    rawCityReport = rawCityReport.map((x) => {
      return {
        city: x.city_name,
        patientCount: x.count,
      };
    });

    rawStateReport = rawStateReport.map((x) => {
      return {
        state: x.state_name,
        patientCount: x.count,
      };
    });

    return Ok({
      cityReport: rawCityReport,
      stateReport: rawStateReport,
    });
  }
}
