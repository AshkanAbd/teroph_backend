import { GetUserReportHandler } from './get-user-report/get-user-report.handler';
import { GetAgeReportHandler } from './get-age-report/get-age-report.handler';
import { GetCityReportHandler } from './get-city-report/get-city-report.handler';

export const QueryHandlers = [GetUserReportHandler, GetAgeReportHandler, GetCityReportHandler];
