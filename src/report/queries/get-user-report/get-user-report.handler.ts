import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetUserReportQuery } from './get-user-report.query';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { GetUserReportDto } from './get-user-report.dto';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { NotFound, NotFoundMsg, Ok } from '../../../common/std-response/std-reponse.factory';

@QueryHandler(GetUserReportQuery)
export class GetUserReportHandler
  extends AbstractHandler
  implements IQueryHandler<GetUserReportQuery, StdResponse<GetUserReportDto>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(query: GetUserReportQuery): Promise<StdResponse<GetUserReportDto>> {
    const patient = await this.repositoryService.patients.findOne(query.id);
    if (!patient) {
      return NotFoundMsg();
    }

    const patientVisits = await this.repositoryService.visits
      .createQueryBuilder()
      .where('patient_id = :patient_id', { patient_id: patient.id })
      .addOrderBy('created_at', 'ASC')
      .getMany();

    return Ok(patientVisits);
  }
}
