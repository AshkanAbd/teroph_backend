import { Module } from '@nestjs/common';
import { ReportController } from './report.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Entities } from '../app.entities';
import { CqrsModule } from '@nestjs/cqrs';
import { AppRepositoryService } from '../services/app-repository.service';
import { QueryHandlers } from './queries';

@Module({
  imports: [TypeOrmModule.forFeature([...Entities]), CqrsModule],
  controllers: [ReportController],
  providers: [AppRepositoryService, ...QueryHandlers],
})
export class ReportModule {
  //
}
