import * as dotenv from 'dotenv';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { useContainer } from 'class-validator';
import { NestExpressApplication } from '@nestjs/platform-express';

async function bootstrap() {
  const config = new DocumentBuilder()
    .setTitle('Teroph documentations')
    .setDescription('Api docs for teroph')
    .setVersion('1.0')
    .addBearerAuth()
    .build();

  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    cors: {
      origin: '*',
      allowedHeaders: '*',
      methods: '*',
      exposedHeaders: ['Content-Disposition'],
    },
  });

  const document = SwaggerModule.createDocument(app, config, { deepScanRoutes: true });
  SwaggerModule.setup('swagger', app, document);

  useContainer(app.select(AppModule), { fallbackOnErrors: true });

  await app.listen(process.env.APP_PORT);
}

dotenv.config();

bootstrap();
