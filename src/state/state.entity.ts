import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { City } from '../city/city.entity';

@Entity('states')
export class State {
  @PrimaryGeneratedColumn('increment', { type: 'bigint' })
  id: number;
  @Column({ type: 'varchar', length: 200, nullable: false })
  name: string;
  @OneToMany(() => City, (city) => (city.state, city.stateId), {
    cascade: true,
    nullable: false,
    onDelete: 'CASCADE',
  })
  cities: City[];
}
