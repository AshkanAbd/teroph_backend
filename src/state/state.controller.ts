import { Controller, Get, Query, Res } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AbstractController } from '../common/extensions/abstract-controller';
import { ApiPaginationQuery } from '../common/swagger/ApiPaginationQuery';
import { ApiSearchQuery } from '../common/swagger/ApiSearchQuery';
import { ApiPaginationResponse } from '../common/swagger/ApiPaginationResponse';
import { GetStateListDto } from './queries/get-state-list/get-state-list-dto';
import { Response } from 'express';
import { QueryBus } from '@nestjs/cqrs';
import { GetStateListQuery } from './queries/get-state-list/get-state-list-query';

@Controller('state')
@ApiTags('state')
export class StateController extends AbstractController {
  constructor(private queryBus: QueryBus) {
    super();
  }

  @Get()
  @ApiPaginationQuery()
  @ApiSearchQuery()
  @ApiPaginationResponse(GetStateListDto)
  async getStateList(
    @Query('search') search: string,
    @Query('page') page: number,
    @Query('pageSize') pageSize: number,
    @Res() res: Response,
  ) {
    this.base(res, await this.queryBus.execute(new GetStateListQuery(page, pageSize, search)));
  }
}
