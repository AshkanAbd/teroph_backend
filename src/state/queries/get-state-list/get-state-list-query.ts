export class GetStateListQuery {
  page: number;
  pageSize: number;
  search: string;

  constructor(page: number, pageSize: number, search: string) {
    this.page = page;
    this.pageSize = pageSize;
    this.search = search;
  }
}
