import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetStateListQuery } from './get-state-list-query';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { GetStateListDto } from './get-state-list-dto';
import { PaginationDto } from '../../../common/pagination/pagination.dto';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { StdResponse } from '../../../common/std-response/std-response';
import { Ok } from '../../../common/std-response/std-reponse.factory';

@QueryHandler(GetStateListQuery)
export class GetStateListHandler
  extends AbstractHandler
  implements IQueryHandler<GetStateListQuery, StdResponse<PaginationDto<GetStateListDto>>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(query: GetStateListQuery): Promise<StdResponse<PaginationDto<GetStateListDto>>> {
    let queryBuilder = await this.repositoryService.states.createQueryBuilder();

    if (!!query.search) {
      query.search = query.search.toLowerCase();

      queryBuilder = queryBuilder.where(`name ilike :search`, { search: `%${query.search}%` });
    }
    const rawStates = await queryBuilder.pagination({
      page: query.page,
      pageSize: query.pageSize,
    });

    const states = new PaginationDto<GetStateListDto>({
      pageCount: rawStates.pageCount,
      currentPage: rawStates.currentPage,
      currentPageSize: rawStates.currentPageSize,
      total: rawStates.total,
    });
    states.list = rawStates.list.map((x) => {
      return new GetStateListDto(x.id, x.name);
    });

    return Ok(states);
  }
}
