import { Controller, Get, Res, UseGuards } from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { AbstractController } from '../common/extensions/abstract-controller';
import { ApiBearerAuth, ApiExtraModels, ApiTags } from '@nestjs/swagger';
import { AdminAuthGuard } from '../auth/admin/admin-auth.guard';
import { Response } from 'express';
import { GetDashboardDto } from './queries/get-dashboard/get-dashboard.dto';
import { ApiStdResponse } from '../common/swagger/ApiStdResponse';
import { GetDashboardQuery } from './queries/get-dashboard/get-dashboard.query';

@ApiBearerAuth()
@ApiTags('dashboard')
@Controller('dashboard')
@UseGuards(AdminAuthGuard)
export class DashboardController extends AbstractController {
  constructor(private queryBus: QueryBus) {
    super();
  }

  @Get('/')
  @ApiStdResponse(GetDashboardDto)
  @ApiExtraModels(GetDashboardDto)
  async getDashboard(@Res() res: Response) {
    this.base(res, await this.queryBus.execute(new GetDashboardQuery()));
  }
}
