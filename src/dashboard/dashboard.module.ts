import { Module } from '@nestjs/common';
import { DashboardController } from './dashboard.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Entities } from '../app.entities';
import { CqrsModule } from '@nestjs/cqrs';
import { AppRepositoryService } from '../services/app-repository.service';
import { QueryHandlers } from './queries';

@Module({
  imports: [TypeOrmModule.forFeature([...Entities]), CqrsModule],
  controllers: [DashboardController],
  providers: [AppRepositoryService, ...QueryHandlers],
})
export class DashboardModule {
  //
}
