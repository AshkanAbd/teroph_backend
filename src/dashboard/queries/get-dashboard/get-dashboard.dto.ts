import { ApiProperty } from '@nestjs/swagger';
import { GetPatientListDto } from '../../../patient/queries/get-patient-list/get-patient-list.dto';
import { GetVisitListDto } from '../../../visit/queries/get-visit-list/get-visit-list.dto';

export class GetDashboardDto {
  @ApiProperty()
  public doctorCount: number;
  @ApiProperty()
  public patientCount: number;
  @ApiProperty()
  public visitCount: number;
  @ApiProperty()
  public todayVisitCount: number;
  @ApiProperty({ type: () => [GetVisitListDto] })
  public lastVisits: GetVisitListDto[];
  @ApiProperty({ type: () => [GetPatientListDto] })
  public lastPatients: GetPatientListDto[];
}
