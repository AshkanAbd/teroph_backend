import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetDashboardQuery } from './get-dashboard.query';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { GetDashboardDto } from './get-dashboard.dto';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { Ok } from '../../../common/std-response/std-reponse.factory';
import { GetPatientListDto } from '../../../patient/queries/get-patient-list/get-patient-list.dto';
import { GetVisitListDto } from '../../../visit/queries/get-visit-list/get-visit-list.dto';
import { GetVisitDoctorDto, GetVisitPatientDto } from '../../../visit/queries/get-visit/get-visit.dto';

@QueryHandler(GetDashboardQuery)
export class GetDashboardHandler
  extends AbstractHandler
  implements IQueryHandler<GetDashboardQuery, StdResponse<GetDashboardDto>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(query: GetDashboardQuery): Promise<StdResponse<GetDashboardDto>> {
    const getDashboard = new GetDashboardDto();

    const now = new Date();
    const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

    getDashboard.doctorCount = await this.repositoryService.doctors.count();
    getDashboard.patientCount = await this.repositoryService.patients.count();
    getDashboard.visitCount = await this.repositoryService.visits.count();
    getDashboard.todayVisitCount = await this.repositoryService.visitDrugs
      .createQueryBuilder()
      .where('created_at >= :today', { today: today })
      .getCount();

    const lastVisits = await this.repositoryService.visits
      .createQueryBuilder('visits')
      .addOrderBy('visits.created_at', 'DESC')
      .innerJoinAndSelect('visits.doctor', 'doctor')
      .innerJoinAndSelect('visits.patient', 'patient')
      .limit(10)
      .getMany();

    getDashboard.lastVisits = lastVisits.map((x) => {
      const visitDto = new GetVisitListDto();

      visitDto.id = x.id.toString();
      visitDto.createdAt = x.createdAt;
      visitDto.visitNumber = x.visitNumber;
      visitDto.visitDate = x.visitDate;

      visitDto.patient = new GetVisitPatientDto();
      visitDto.patient.id = x.patient.id.toString();
      visitDto.patient.fullName = `${x.patient.firstname} ${x.patient.lastname}`;
      visitDto.patient.nationalId = x.patient.nationalId;
      visitDto.patient.recordNumber = x.patient.recordNumber;

      visitDto.doctor = new GetVisitDoctorDto();
      visitDto.doctor.id = x.doctor.id.toString();
      visitDto.doctor.fullName = x.doctor.fullName;
      visitDto.doctor.medicalNumber = x.doctor.medicalNumber;

      return visitDto;
    });

    const lastPatients = await this.repositoryService.patients
      .createQueryBuilder()
      .addOrderBy('created_at', 'DESC')
      .limit(10)
      .getMany();

    getDashboard.lastPatients = lastPatients.map((x) => {
      return new GetPatientListDto(
        x.id,
        `${x.firstname} ${x.lastname}`,
        x.phone,
        x.nationalId,
        x.recordNumber,
        x.diseaseAge,
      );
    });

    return Ok(getDashboard);
  }
}
