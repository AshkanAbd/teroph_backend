import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { VisitDrug } from '../visit-drug/visit-drug.entity';

@Entity('drugs', { name: 'drugs' })
export class Drug {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'id' })
  public id: number;
  @Column({ type: 'varchar', name: 'name', length: 100, nullable: false })
  public name: string;
  @Column({ type: 'varchar', name: 'doz', length: 100, nullable: false })
  public doz: string;
  @CreateDateColumn({ type: 'timestamp', name: 'created_at', nullable: false })
  public createdAt: Date;
  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at', nullable: false })
  public updatedAt: Date;
  @DeleteDateColumn({ type: 'timestamp', name: 'deleted_at', nullable: true })
  public deletedAt: Date;

  @OneToMany(() => VisitDrug, (visitDrug) => visitDrug.drug, {
    cascade: true,
    nullable: false,
    onDelete: 'CASCADE',
  })
  visitDrug: VisitDrug[];
}
