import { ApiProperty } from '@nestjs/swagger';

export class GetDrugDto {
  @ApiProperty()
  public id: number;
  @ApiProperty()
  public name: string;
  @ApiProperty()
  public doz: string;

  constructor(id: number, name: string, doz: string) {
    this.id = id;
    this.name = name;
    this.doz = doz;
  }
}
