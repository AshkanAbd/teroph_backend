import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetDrugQuery } from './get-drug.query';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { GetDrugDto } from './get-drug.dto';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { NotFoundMsg, Ok } from '../../../common/std-response/std-reponse.factory';

@QueryHandler(GetDrugQuery)
export class GetDrugHandler extends AbstractHandler implements IQueryHandler<GetDrugQuery, StdResponse<GetDrugDto>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(query: GetDrugQuery): Promise<StdResponse<GetDrugDto>> {
    const drug = await this.repositoryService.drugs.findOne(query.id);

    if (!drug) {
      return NotFoundMsg();
    }

    return Ok(new GetDrugDto(drug.id, drug.name, drug.doz));
  }
}
