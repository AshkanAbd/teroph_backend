import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetDrugListQuery } from './get-drug-list.query';
import { StdResponse } from '../../../common/std-response/std-response';
import { PaginationDto } from '../../../common/pagination/pagination.dto';
import { GetDrugDto } from '../get-drug/get-drug.dto';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { Ok } from '../../../common/std-response/std-reponse.factory';

@QueryHandler(GetDrugListQuery)
export class GetDrugListHandler
  extends AbstractHandler
  implements IQueryHandler<GetDrugListQuery, StdResponse<PaginationDto<GetDrugDto>>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(query: GetDrugListQuery): Promise<StdResponse<PaginationDto<GetDrugDto>>> {
    let queryBuilder = this.repositoryService.drugs.createQueryBuilder('a').orderBy('created_at', 'DESC');

    if (!!query.search) {
      query.search = query.search.toLowerCase();

      queryBuilder = queryBuilder.where(`name ilike :search`, { search: `%${query.search}%` });
    }

    const rawDrugs = await queryBuilder.pagination({
      page: query.page,
      pageSize: query.pageSize,
    });

    const drugs = new PaginationDto<GetDrugDto>({
      pageCount: rawDrugs.pageCount,
      currentPage: rawDrugs.currentPage,
      currentPageSize: rawDrugs.currentPageSize,
      total: rawDrugs.total,
    });
    drugs.list = rawDrugs.list.map((x) => {
      return new GetDrugDto(x.id, x.name, x.doz);
    });

    return Ok(drugs);
  }
}
