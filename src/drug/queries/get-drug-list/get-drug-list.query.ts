export class GetDrugListQuery {
  search: string;
  page: number;
  pageSize: number;

  constructor(search: string, page: number, pageSize: number) {
    this.search = search;
    this.page = page;
    this.pageSize = pageSize;
  }
}
