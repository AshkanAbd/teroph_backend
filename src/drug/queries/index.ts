import { GetDrugHandler } from './get-drug/get-drug.handler';
import { GetDrugListHandler } from './get-drug-list/get-drug-list.handler';

export const QueryHandlers = [GetDrugHandler, GetDrugListHandler];
