import { Module } from '@nestjs/common';
import { DrugController } from './drug.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Entities } from '../app.entities';
import { CqrsModule } from '@nestjs/cqrs';
import { AppRepositoryService } from '../services/app-repository.service';
import { QueryHandlers } from './queries';
import { CommandHandlers } from './commands';

@Module({
  imports: [TypeOrmModule.forFeature([...Entities]), CqrsModule],
  controllers: [DrugController],
  providers: [AppRepositoryService, ...QueryHandlers, ...CommandHandlers],
})
export class DrugModule {
  //
}
