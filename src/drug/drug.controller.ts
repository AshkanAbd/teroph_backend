import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Res,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiExtraModels, ApiParam, ApiTags } from '@nestjs/swagger';
import { AbstractController } from '../common/extensions/abstract-controller';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiStdResponse } from '../common/swagger/ApiStdResponse';
import { GetDrugDto } from './queries/get-drug/get-drug.dto';
import { NotFoundStdException } from '../common/std-response/std-reponse.factory';
import { Response } from 'express';
import { GetDrugQuery } from './queries/get-drug/get-drug.query';
import { ApiSearchQuery } from '../common/swagger/ApiSearchQuery';
import { ApiPaginationQuery } from '../common/swagger/ApiPaginationQuery';
import { ApiPaginationResponse } from '../common/swagger/ApiPaginationResponse';
import { GetDrugListQuery } from './queries/get-drug-list/get-drug-list.query';
import { CreateDrugCommand } from './commands/create-drug/create-drug.command';
import { TransformerPipe } from '../common/pipes/transformer.pipe';
import { UpdateDrugCommand } from './commands/update-drug/update-drug.command';
import { DeleteDrugCommand } from './commands/delete-drug/delete-drug.command';
import { AdminAuthGuard } from '../auth/admin/admin-auth.guard';

@ApiBearerAuth()
@ApiTags('drug')
@Controller('drug')
@UseGuards(AdminAuthGuard)
export class DrugController extends AbstractController {
  constructor(private queryBus: QueryBus, private commandBus: CommandBus) {
    super();
  }

  @Get()
  @ApiPaginationQuery()
  @ApiSearchQuery()
  @ApiPaginationResponse(GetDrugDto)
  async getDrugList(
    @Query('search') search: string,
    @Query('page') page: number,
    @Query('pageSize') pageSize: number,
    @Res() res: Response,
  ) {
    this.base(res, await this.queryBus.execute(new GetDrugListQuery(search, page, pageSize)));
  }

  @Post()
  @ApiBody({ type: CreateDrugCommand })
  @ApiStdResponse(GetDrugDto)
  @UsePipes(new TransformerPipe('body'))
  async createDrug(@Body() createDrugCommand: CreateDrugCommand, @Res() res: Response) {
    this.base(res, await this.commandBus.execute(createDrugCommand));
  }

  @Get('/:id')
  @ApiParam({ name: 'id', type: Number, required: true })
  @ApiStdResponse(GetDrugDto)
  @ApiExtraModels(GetDrugDto)
  async getDrug(
    @Param('id', new ParseIntPipe({ exceptionFactory: NotFoundStdException })) id: number,
    @Res() res: Response,
  ) {
    this.base(res, await this.queryBus.execute(new GetDrugQuery(id)));
  }

  @Put('/:id')
  @ApiParam({ name: 'id', type: Number, required: true })
  @ApiBody({ type: UpdateDrugCommand })
  @ApiStdResponse(GetDrugDto)
  @UsePipes(new TransformerPipe('body'))
  async updateDrug(
    @Param('id', new ParseIntPipe({ exceptionFactory: NotFoundStdException })) id: number,
    @Body() updateDrugCommand: UpdateDrugCommand,
    @Res() res: Response,
  ) {
    this.base(res, await this.commandBus.execute(updateDrugCommand));
  }

  @Delete('/:id')
  @ApiParam({ name: 'id', type: Number, required: true })
  @ApiPaginationResponse(GetDrugDto)
  async deleteDrug(
    @Param('id', new ParseIntPipe({ exceptionFactory: NotFoundStdException })) id: number,
    @Res() res: Response,
  ) {
    this.base(res, await this.commandBus.execute(new DeleteDrugCommand(id)));
  }
}
