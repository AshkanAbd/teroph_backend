import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';

export class CreateDrugCommand {
  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  @MinLength(1, { message: '' })
  @MaxLength(100, { message: '' })
  @ApiProperty()
  public name: string;
  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  @MinLength(1, { message: '' })
  @MaxLength(100, { message: '' })
  @ApiProperty()
  public doz: string;
}
