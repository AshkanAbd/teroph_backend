import { CreateDrugHandler } from './create-drug/create-drug.handler';
import { UpdateDrugHandler } from './update-drug/update-drug.handler';
import { DeleteDrugHandler } from './delete-drug/delete-drug.handler';

export const CommandHandlers = [CreateDrugHandler, UpdateDrugHandler, DeleteDrugHandler];
