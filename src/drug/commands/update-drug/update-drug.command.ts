import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateDrugCommand {
  id: number;

  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  @MinLength(1, { message: '' })
  @MaxLength(100, { message: '' })
  @ApiProperty()
  public name: string;

  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  @MinLength(1, { message: '' })
  @MaxLength(100, { message: '' })
  @ApiProperty()
  public doz: string;
}
