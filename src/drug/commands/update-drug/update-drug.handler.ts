import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { UpdateDrugCommand } from './update-drug.command';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { GetDrugDto } from '../../queries/get-drug/get-drug.dto';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { BadRequest, NotFoundMsg, Ok } from '../../../common/std-response/std-reponse.factory';
import { Validator } from '../../../common/validations/validator';

@CommandHandler(UpdateDrugCommand)
export class UpdateDrugHandler
  extends AbstractHandler
  implements ICommandHandler<UpdateDrugCommand, StdResponse<GetDrugDto>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(command: UpdateDrugCommand): Promise<StdResponse<GetDrugDto>> {
    const drug = await this.repositoryService.drugs.findOne(command.id);

    if (!drug) {
      return NotFoundMsg();
    }

    const validationResult = await Validator.validate(command);
    if (validationResult.failed()) {
      return BadRequest<any>(validationResult.getMessage());
    }

    drug.name = command.name;
    drug.doz = command.doz;

    await this.repositoryService.drugs.save(drug);

    return Ok(new GetDrugDto(drug.id, drug.name, drug.doz), 'Drug updated successfully.');
  }
}
