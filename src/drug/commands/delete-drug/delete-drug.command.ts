export class DeleteDrugCommand {
  id: number;

  constructor(id: number) {
    this.id = id;
  }
}
