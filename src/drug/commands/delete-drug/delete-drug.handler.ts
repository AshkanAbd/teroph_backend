import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { DeleteDrugCommand } from './delete-drug.command';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { PaginationDto } from '../../../common/pagination/pagination.dto';
import { GetDrugDto } from '../../queries/get-drug/get-drug.dto';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { NotFound } from '../../../common/std-response/std-reponse.factory';
import { GetDrugListHandler } from '../../queries/get-drug-list/get-drug-list.handler';
import { GetDrugListQuery } from '../../queries/get-drug-list/get-drug-list.query';

@CommandHandler(DeleteDrugCommand)
export class DeleteDrugHandler
  extends AbstractHandler
  implements ICommandHandler<DeleteDrugCommand, StdResponse<PaginationDto<GetDrugDto>>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(command: DeleteDrugCommand): Promise<StdResponse<PaginationDto<GetDrugDto>>> {
    const drug = await this.repositoryService.drugs.findOne(command.id);

    if (!drug) {
      return NotFound();
    }

    await this.repositoryService.drugs.softRemove(drug);

    const getDrugListHandler = new GetDrugListHandler(this.repositoryService, this.moduleRef);
    const getDrugList = await getDrugListHandler.execute(new GetDrugListQuery(null, 1, 10));
    getDrugList.message = 'Drug deleted successfully.';

    return getDrugList;
  }
}
