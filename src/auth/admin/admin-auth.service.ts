import { ExtractJwt, Strategy as JwtStrategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { AppRepositoryService } from '../../services/app-repository.service';
import { NotAuthStdException } from '../../common/std-response/std-reponse.factory';

@Injectable()
export class AdminAuthService extends PassportStrategy(JwtStrategy, 'admin') {
  constructor(private appRepositoryService: AppRepositoryService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate(payload: any) {
    const userId = payload.id;

    const admin = await this.appRepositoryService.admins.findOne(userId);

    if (!admin) {
      throw NotAuthStdException();
    }

    return admin;
  }
}
