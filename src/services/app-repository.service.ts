import { Injectable, Scope } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Patient } from '../patient/patient.entity';
import { Repository } from 'typeorm';
import { Doctor } from '../doctor/doctor.entity';
import { Drug } from '../drug/drug.entity';
import { Admin } from '../admin/admin.entity';
import { Visit } from '../visit/visit.entity';
import { VisitDrug } from '../visit-drug/visit-drug.entity';
import { State } from '../state/state.entity';
import { City } from '../city/city.entity';

@Injectable({
  scope: Scope.TRANSIENT,
})
export class AppRepositoryService {
  constructor(
    @InjectRepository(Patient)
    public patients: Repository<Patient>,
    @InjectRepository(Doctor)
    public doctors: Repository<Doctor>,
    @InjectRepository(Drug)
    public drugs: Repository<Drug>,
    @InjectRepository(Admin)
    public admins: Repository<Admin>,
    @InjectRepository(Visit)
    public visits: Repository<Visit>,
    @InjectRepository(VisitDrug)
    public visitDrugs: Repository<VisitDrug>,
    @InjectRepository(State)
    public states: Repository<State>,
    @InjectRepository(City)
    public cities: Repository<City>,
  ) {
    //
  }
}
