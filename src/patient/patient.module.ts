import { Module } from '@nestjs/common';
import { PatientController } from './patient.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CqrsModule } from '@nestjs/cqrs';
import { AppRepositoryService } from '../services/app-repository.service';
import { CommandHandlers } from './commands';
import { QueryHandlers } from './queries';
import { Entities } from '../app.entities';

@Module({
  imports: [TypeOrmModule.forFeature([...Entities]), CqrsModule],
  controllers: [PatientController],
  providers: [AppRepositoryService, ...CommandHandlers, ...QueryHandlers],
})
export class PatientModule {
  //
}
