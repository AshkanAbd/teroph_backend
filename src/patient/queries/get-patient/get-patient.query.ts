export class GetPatientQuery {
  public id: number;

  constructor(id: number) {
    this.id = id;
  }
}
