import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetPatientQuery } from './get-patient.query';
import { GetPatientDto } from './get-patient.dto';
import { StdResponse } from '../../../common/std-response/std-response';
import { NotFoundMsg, Ok } from '../../../common/std-response/std-reponse.factory';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';

@QueryHandler(GetPatientQuery)
export class GetPatientHandler
  extends AbstractHandler
  implements IQueryHandler<GetPatientQuery, StdResponse<GetPatientDto>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(query: GetPatientQuery): Promise<StdResponse<GetPatientDto>> {
    const patient = await this.repositoryService.patients.findOne(query.id);

    if (!patient) {
      return NotFoundMsg();
    }

    let city, state;
    if (!!patient.cityId) {
      city = await this.repositoryService.cities.findOne(patient.cityId);
      state = await this.repositoryService.states.findOne(city.stateId);
    }

    return Ok<GetPatientDto>({
      id: typeof patient.id === 'number' ? patient.id : parseInt(patient.id),
      phone: patient.phone,
      firstname: patient.firstname,
      lastname: patient.lastname,
      fatherName: patient.fatherName,
      nationalId: patient.nationalId,
      recordNumber: patient.recordNumber,
      birthdate: patient.birthdate,
      address: patient.address,
      diseaseAge: patient.diseaseAge,
      city: !!patient.cityId
        ? {
          id: city.id,
          name: city.name,
          state: {
            id: state.id,
            name: state.name,
          },
        }
        : null,
    });
  }
}
