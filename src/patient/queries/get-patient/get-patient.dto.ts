import { ApiProperty } from '@nestjs/swagger';

export class GetPatientCityStateDto {
  @ApiProperty()
  id: number;
  @ApiProperty()
  name: string;
}

export class GetPatientCityDto {
  @ApiProperty()
  id: number;
  @ApiProperty()
  name: string;
  @ApiProperty()
  state: GetPatientCityStateDto;
}

export class GetPatientDto {
  @ApiProperty()
  id: number;
  @ApiProperty()
  phone: string;
  @ApiProperty()
  firstname: string;
  @ApiProperty()
  lastname: string;
  @ApiProperty()
  fatherName: string;
  @ApiProperty()
  nationalId: string;
  @ApiProperty()
  recordNumber: string;
  @ApiProperty()
  birthdate: Date;
  @ApiProperty()
  address: string;
  @ApiProperty()
  diseaseAge: number;
  @ApiProperty()
  city: GetPatientCityDto;
}
