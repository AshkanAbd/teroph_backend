import { isString, maxLength, minLength } from 'class-validator';
import { MustSync, NotNull } from '../../../common/validations/custom-validation/must-sync.rule';

export class GetPatientListQuery {
  @MustSync((_, x) => isString(x), NotNull)
  @MustSync((_, x) => minLength(x, 1), NotNull)
  @MustSync((_, x) => maxLength(x, 50), NotNull)
  public search: string;
  public page: number;
  public pageSize: number;
  public cityId: string;

  constructor(search: string, page: number, pageSize: number, cityId: string) {
    this.search = search;
    this.page = page;
    this.pageSize = pageSize;
    this.cityId = cityId;
  }
}
