import { ApiProperty } from '@nestjs/swagger';

export class GetPatientListDto {
  @ApiProperty()
  public id: number;
  @ApiProperty()
  public fullName: string;
  @ApiProperty()
  public phone: string;
  @ApiProperty()
  public nationalId: string;
  @ApiProperty()
  public recordNumber: string;
  @ApiProperty()
  public diseaseAge: number;

  constructor(
    id: number,
    fullName: string,
    phone: string,
    nationId: string,
    recordNumber: string,
    diseaseAge: number,
  ) {
    this.id = id;
    this.fullName = fullName;
    this.phone = phone;
    this.nationalId = nationId;
    this.recordNumber = recordNumber;
    this.diseaseAge = diseaseAge;
  }
}
