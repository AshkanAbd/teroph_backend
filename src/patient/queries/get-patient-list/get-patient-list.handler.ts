import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetPatientListQuery } from './get-patient-list.query';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { GetPatientListDto } from './get-patient-list.dto';
import { BadRequest, Ok } from '../../../common/std-response/std-reponse.factory';
import { PaginationDto } from '../../../common/pagination/pagination.dto';
import { Validator } from '../../../common/validations/validator';
import { Brackets } from 'typeorm';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';

@QueryHandler(GetPatientListQuery)
export class GetPatientListHandler
  extends AbstractHandler
  implements IQueryHandler<GetPatientListQuery, StdResponse<PaginationDto<GetPatientListDto>>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(query: GetPatientListQuery): Promise<StdResponse<PaginationDto<GetPatientListDto>>> {
    const validationResult = await Validator.validate(query);
    if (validationResult.failed()) {
      return BadRequest<any>(validationResult.getMessage());
    }

    let queryBuilder = this.repositoryService.patients.createQueryBuilder().orderBy('created_at', 'DESC');

    if (!!query.search) {
      query.search = query.search.toLowerCase();

      queryBuilder = queryBuilder.where(
        new Brackets((q) => {
          q.where('phone ilike :search', { search: `%${query.search}%` })
            .orWhere('firstname ilike :search', { search: `%${query.search}%` })
            .orWhere('lastname ilike :search', { search: `%${query.search}%` })
            .orWhere('record_number ilike :search', { search: `%${query.search}%` });
        }),
      );
    }

    if (!!parseInt(query.cityId)) {
      queryBuilder = queryBuilder.where('city_id = :city_id', { city_id: query.cityId });
    }

    const rawPatients = await queryBuilder.pagination({
      page: query.page,
      pageSize: query.pageSize,
    });

    const patients = new PaginationDto<GetPatientListDto>();

    patients.list = rawPatients.list.map(
      (x) =>
        new GetPatientListDto(
          x.id,
          `${x.firstname} ${x.lastname}`,
          x.phone,
          x.nationalId,
          x.recordNumber,
          x.diseaseAge,
        ),
    );
    patients.pageCount = rawPatients.pageCount;
    patients.currentPage = rawPatients.currentPage;
    patients.currentPageSize = rawPatients.currentPageSize;
    patients.total = rawPatients.total;

    return Ok(patients);
  }
}
