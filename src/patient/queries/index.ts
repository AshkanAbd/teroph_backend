import { GetPatientHandler } from './get-patient/get-patient.handler';
import { GetPatientListHandler } from './get-patient-list/get-patient-list.handler';

export const QueryHandlers = [GetPatientHandler, GetPatientListHandler];
