import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Res,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { CreatePatientCommand } from './commands/create-patient/create-patient.command';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiBearerAuth, ApiBody, ApiExtraModels, ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import { TransformerPipe } from '../common/pipes/transformer.pipe';
import { AbstractController } from '../common/extensions/abstract-controller';
import { Response } from 'express';
import { StdResponse } from '../common/std-response/std-response';
import { ApiStdResponse } from '../common/swagger/ApiStdResponse';
import { GetPatientQuery } from './queries/get-patient/get-patient.query';
import { GetPatientDto } from './queries/get-patient/get-patient.dto';
import { NotFoundStdException } from '../common/std-response/std-reponse.factory';
import { PaginationDto } from '../common/pagination/pagination.dto';
import { ApiPaginationResponse } from '../common/swagger/ApiPaginationResponse';
import { GetPatientListDto } from './queries/get-patient-list/get-patient-list.dto';
import { GetPatientListQuery } from './queries/get-patient-list/get-patient-list.query';
import { UpdatePatientCommand } from './commands/update-patient/update-patient.command';
import { DeletePatientCommand } from './commands/delete-patient/delete-patient.command';
import { ApiPaginationQuery } from '../common/swagger/ApiPaginationQuery';
import { ApiSearchQuery } from '../common/swagger/ApiSearchQuery';
import { AdminAuthGuard } from '../auth/admin/admin-auth.guard';

@ApiBearerAuth()
@ApiTags('patient')
@Controller('patient')
@UseGuards(AdminAuthGuard)
@ApiExtraModels(StdResponse, PaginationDto)
export class PatientController extends AbstractController {
  constructor(private readonly commandBus: CommandBus, private readonly queryBus: QueryBus) {
    super();
  }

  @Get()
  @ApiPaginationQuery()
  @ApiSearchQuery()
  @ApiQuery({ required: false, name: 'cityId' })
  @ApiPaginationResponse(GetPatientListDto)
  @ApiExtraModels(GetPatientListDto)
  async getPatientList(
    @Res() res: Response,
    @Query('search') search: string,
    @Query('page') page: number,
    @Query('pageSize') pageSize: number,
    @Query('cityId') cityId: string,
  ) {
    this.base(res, await this.queryBus.execute(new GetPatientListQuery(search, page, pageSize, cityId)));
  }

  @Post('/')
  @ApiBody({
    type: CreatePatientCommand,
  })
  @ApiStdResponse(GetPatientDto)
  @UsePipes(new TransformerPipe('body'))
  async create(@Body() createPatientCommand: CreatePatientCommand, @Res() res: Response) {
    this.base(res, await this.commandBus.execute(createPatientCommand));
  }

  @Get('/:id')
  @ApiParam({ name: 'id' })
  @ApiStdResponse(GetPatientDto)
  @ApiExtraModels(GetPatientDto)
  async get(
    @Param('id', new ParseIntPipe({ exceptionFactory: NotFoundStdException })) id: number,
    @Res() res: Response,
  ) {
    this.base(res, await this.queryBus.execute(new GetPatientQuery(id)));
  }

  @Put('/:id')
  @ApiBody({
    type: UpdatePatientCommand,
  })
  @ApiParam({ name: 'id' })
  @ApiStdResponse(GetPatientDto)
  @UsePipes(new TransformerPipe('body'))
  async update(
    @Param('id', new ParseIntPipe({ exceptionFactory: NotFoundStdException })) id: number,
    @Body() updatePatientCommand: UpdatePatientCommand,
    @Res() res: Response,
  ) {
    updatePatientCommand.id = id;
    this.base(res, await this.commandBus.execute(updatePatientCommand));
  }

  @Delete('/:id')
  @ApiParam({ name: 'id' })
  @ApiPaginationResponse(GetPatientListDto)
  async delete(
    @Param('id', new ParseIntPipe({ exceptionFactory: NotFoundStdException })) id: number,
    @Res() res: Response,
  ) {
    this.base(res, await this.commandBus.execute(new DeletePatientCommand(id)));
  }
}
