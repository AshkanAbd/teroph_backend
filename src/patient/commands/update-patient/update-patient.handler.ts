import { CommandHandler, ICommandHandler, QueryBus } from '@nestjs/cqrs';
import { UpdatePatientCommand } from './update-patient.command';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { GetPatientDto } from '../../queries/get-patient/get-patient.dto';
import { BadRequest, NotFoundMsg, Ok } from '../../../common/std-response/std-reponse.factory';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { Validator } from '../../../common/validations/validator';

@CommandHandler(UpdatePatientCommand)
export class UpdatePatientHandler
  extends AbstractHandler
  implements ICommandHandler<UpdatePatientCommand, StdResponse<GetPatientDto>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(command: UpdatePatientCommand): Promise<StdResponse<GetPatientDto>> {
    const patient = await this.repositoryService.patients.findOne(command.id);
    if (!patient) {
      return NotFoundMsg();
    }

    const validationResult = await Validator.validate(command);
    if (validationResult.failed()) {
      return BadRequest<any>(validationResult.getMessage());
    }

    patient.phone = command.phone;
    patient.firstname = command.firstname;
    patient.lastname = command.lastname;
    patient.fatherName = command.fatherName;
    patient.nationalId = command.nationalId;
    patient.recordNumber = command.recordNumber;
    patient.birthdate = command.birthdate;
    patient.address = command.address;
    patient.diseaseAge = command.diseaseAge;
    patient.cityId = command.cityId;

    await this.repositoryService.patients.save(patient);

    const city = await this.repositoryService.cities.findOne(patient.cityId);
    const state = await this.repositoryService.states.findOne(city.stateId);

    return Ok(
      {
        id: patient.id,
        phone: patient.phone,
        firstname: patient.firstname,
        lastname: patient.lastname,
        fatherName: patient.fatherName,
        nationalId: patient.nationalId,
        recordNumber: patient.recordNumber,
        birthdate: patient.birthdate,
        address: patient.address,
        diseaseAge: patient.diseaseAge,
        city: {
          id: city.id,
          name: city.name,
          state: {
            id: state.id,
            name: state.name,
          },
        },
      },
      'Patient updated successfully.',
    );
  }
}
