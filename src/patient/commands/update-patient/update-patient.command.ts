import {
  IsDateString,
  IsInt,
  IsNotEmpty,
  IsPhoneNumber,
  IsString,
  Matches,
  Max,
  MaxLength,
  Min,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Must, NotNullAsync } from '../../../common/validations/custom-validation/must.rule';
import { ModuleRef } from '@nestjs/core';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { MustSync, NotNull } from '../../../common/validations/custom-validation/must-sync.rule';

export class UpdatePatientCommand {
  id: number;

  @IsString({ message: '' })
  @IsPhoneNumber('IR', { message: '' })
  @IsNotEmpty({ message: '' })
  @ApiProperty()
  @Must(
    async (moduleRef: ModuleRef, x: string, y: UpdatePatientCommand) => {
      const repos = await moduleRef.resolve(AppRepositoryService);
      return (
        (await repos.patients
          .createQueryBuilder()
          .where('phone = :phone', { phone: x })
          .andWhere('id != :id', { id: y.id })
          .getCount()) == 0
      );
    },
    NotNullAsync,
    { message: 'A user with this phone number exists in the system' },
  )
  public phone: string;

  @IsString({ message: '' })
  @IsNotEmpty({ message: '' })
  @MaxLength(1000, { message: '' })
  @MinLength(1, { message: '' })
  @ApiProperty()
  firstname: string;

  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  @MaxLength(1000, { message: '' })
  @MinLength(1, { message: '' })
  @ApiProperty()
  lastname: string;

  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  @MaxLength(1000, { message: '' })
  @MinLength(1, { message: '' })
  @ApiProperty()
  fatherName: string;

  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  @Matches(new RegExp('^[0-9]{10}$'), { message: '' })
  @Must(
    async (moduleRef: ModuleRef, x: string, y: UpdatePatientCommand) => {
      const repos = await moduleRef.resolve(AppRepositoryService);
      return (
        (await repos.patients
          .createQueryBuilder()
          .where('national_id = :nationalId', { nationalId: x })
          .andWhere('id != :id', { id: y.id })
          .getCount()) === 0
      );
    },
    NotNullAsync,
    { message: 'A user with this national code exists in the system' },
  )
  @ApiProperty()
  nationalId: string;

  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  @Matches(new RegExp('^[0-9]+$'), { message: '' })
  @ApiProperty()
  recordNumber: string;

  @IsNotEmpty({ message: '' })
  @IsDateString()
  @MustSync((_, x) => Date.parse(x) >= new Date(1900, 1, 1).getTime(), NotNull, { message: '' })
  @MustSync((_, x) => Date.parse(x) <= Date.now(), NotNull, { message: '' })
  @ApiProperty()
  birthdate: Date;

  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  @MinLength(1, { message: '' })
  @MaxLength(1000, { message: '' })
  @ApiProperty()
  address: string;

  @IsNotEmpty({ message: '' })
  @IsInt({ message: '' })
  @Min(1, { message: '' })
  @Max(1000, { message: '' })
  @ApiProperty()
  diseaseAge: number;

  @IsNotEmpty({ message: 'شهر بیمار خالی است.' })
  @IsInt({ message: 'شهر بیمار معتبر نیست.' })
  @Must(
    async (moduleRef: ModuleRef, x: string) => {
      const repos = await moduleRef.resolve(AppRepositoryService);
      return (await repos.cities.createQueryBuilder().where('id = :city_id', { city_id: x }).getCount()) === 1;
    },
    NotNullAsync,
    { message: 'شهر بیمار معتبر نیست.' },
  )
  @ApiProperty()
  cityId: number;
}
