import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { DeletePatientCommand } from './delete-patient.command';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { PaginationDto } from '../../../common/pagination/pagination.dto';
import { GetPatientListDto } from '../../queries/get-patient-list/get-patient-list.dto';
import { NotFoundMsg, Ok } from '../../../common/std-response/std-reponse.factory';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { GetPatientListHandler } from '../../queries/get-patient-list/get-patient-list.handler';
import { GetPatientListQuery } from '../../queries/get-patient-list/get-patient-list.query';

@CommandHandler(DeletePatientCommand)
export class DeletePatientHandler
  extends AbstractHandler
  implements ICommandHandler<DeletePatientCommand, StdResponse<PaginationDto<GetPatientListDto>>> {

  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(command: DeletePatientCommand): Promise<StdResponse<PaginationDto<GetPatientListDto>>> {
    const patient = await this.repositoryService.patients.findOne(command.id);

    if (!patient) {
      return NotFoundMsg();
    }

    await this.repositoryService.patients.softRemove(patient);

    const getPatientListHandler = new GetPatientListHandler(this.repositoryService, this.moduleRef);
    const getPatientList = await getPatientListHandler.execute(new GetPatientListQuery(null, 1, 10, null));
    getPatientList.message = 'Patient deleted successfully.';
    return getPatientList;
  }
}
