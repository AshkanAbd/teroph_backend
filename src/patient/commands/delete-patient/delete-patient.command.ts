export class DeletePatientCommand {
  id: number;

  constructor(id: number) {
    this.id = id;
  }
}
