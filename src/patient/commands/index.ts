import { CreatePatientHandler } from './create-patient/create-patient.handler';
import { UpdatePatientHandler } from './update-patient/update-patient.handler';
import { DeletePatientHandler } from './delete-patient/delete-patient.handler';

export const CommandHandlers = [CreatePatientHandler, UpdatePatientHandler, DeletePatientHandler];
