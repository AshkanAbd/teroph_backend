import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { CreatePatientCommand } from './create-patient.command';
import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { StdResponse } from '../../../common/std-response/std-response';
import { BadRequest, Ok } from '../../../common/std-response/std-reponse.factory';
import { Validator } from '../../../common/validations/validator';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { GetPatientDto } from '../../queries/get-patient/get-patient.dto';

@CommandHandler(CreatePatientCommand)
export class CreatePatientHandler
  extends AbstractHandler
  implements ICommandHandler<CreatePatientCommand, StdResponse<GetPatientDto>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef) {
    super(repositoryService, moduleRef);
  }

  async execute(command: CreatePatientCommand): Promise<StdResponse<GetPatientDto>> {
    const validationResult = await Validator.validate(command);
    if (validationResult.failed()) {
      return BadRequest<any>(validationResult.getMessage());
    }

    let patient = this.repositoryService.patients.create({
      phone: command.phone,
      firstname: command.firstname,
      lastname: command.lastname,
      fatherName: command.fatherName,
      nationalId: command.nationalId,
      recordNumber: command.recordNumber,
      birthdate: command.birthdate,
      address: command.address,
      diseaseAge: command.diseaseAge,
      cityId: command.cityId,
    });

    patient = await this.repositoryService.patients.save(patient);

    const city = await this.repositoryService.cities.findOne(patient.cityId);
    const state = await this.repositoryService.states.findOne(city.stateId);

    return Ok(
      {
        id: patient.id,
        phone: patient.phone,
        firstname: patient.firstname,
        lastname: patient.lastname,
        fatherName: patient.fatherName,
        nationalId: patient.nationalId,
        recordNumber: patient.recordNumber,
        birthdate: patient.birthdate,
        address: patient.address,
        diseaseAge: patient.diseaseAge,
        city: {
          id: city.id,
          name: city.name,
          state: {
            id: state.id,
            name: state.name,
          },
        },
      },
      'Patient created successfully.',
    );
  }
}
