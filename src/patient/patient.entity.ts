import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  RelationId,
  UpdateDateColumn,
} from 'typeorm';
import { Visit } from '../visit/visit.entity';
import { City } from '../city/city.entity';

@Entity('patients')
export class Patient {
  @PrimaryGeneratedColumn('increment', { type: 'bigint' })
  id: number;
  @Column({ type: 'text', nullable: true })
  phone: string;
  @Column({ type: 'text', nullable: true, name: 'firstname' })
  firstname: string;
  @Column({ type: 'text', nullable: true, name: 'lastname' })
  lastname: string;
  @Column({ type: 'text', nullable: true, name: 'father_name' })
  fatherName: string;
  @Column({ type: 'text', nullable: true, name: 'national_id' })
  nationalId: string;
  @Column({ type: 'text', nullable: true, name: 'record_number' })
  recordNumber: string;
  @Column({ type: 'timestamp', nullable: true, name: 'birth_date' })
  birthdate: Date;
  @Column({ type: 'text', nullable: true, name: 'address' })
  address: string;
  @Column({ type: 'int', nullable: true, name: 'diseases_age' })
  diseaseAge: number;
  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;
  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
  @DeleteDateColumn({ name: 'deleted_at' })
  deletedAt: Date;
  @RelationId((object: Patient) => object.city)
  @Column({ type: 'bigint', name: 'city_id', nullable: true })
  cityId: number;
  @OneToMany(() => Visit, (visit) => (visit.patient, visit.patientId), {
    cascade: true,
    nullable: false,
    onDelete: 'CASCADE',
  })
  visits: Visit[];
  @ManyToOne(() => City, (city) => city.patients)
  @JoinColumn({ name: 'city_id' })
  city: City;
}
