import { Column, CreateDateColumn, DeleteDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity('admins')
export class Admin {
  @PrimaryGeneratedColumn('increment', { type: 'bigint', name: 'id' })
  public id: number;
  @Column({ type: 'varchar', name: 'phone', length: 11, nullable: false })
  public phone: string;
  @Column({ type: 'varchar', name: 'password', length: 500, nullable: false })
  public password: string;
  @Column({ type: 'varchar', name: 'full_name', length: 100, nullable: false })
  public fullName: string;
  @CreateDateColumn({ type: 'timestamp', name: 'created_at', default: 'now()', nullable: false })
  public createdAt: Date;
  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at', default: 'now()', nullable: false })
  public updatedAt: Date;
  @DeleteDateColumn({ type: 'timestamp', name: 'deleted_at', nullable: true })
  public deletedAt: Date;
}
