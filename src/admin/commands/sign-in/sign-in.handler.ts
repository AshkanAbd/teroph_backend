import { AbstractHandler } from '../../../common/handlers/abstract.handler';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { SignInCommand } from './sign-in.command';
import { StdResponse } from '../../../common/std-response/std-response';
import { SignInDto } from './sign-in.dto';
import { BadRequest, BadRequestMsg, NotFound, Ok } from '../../../common/std-response/std-reponse.factory';
import { AppRepositoryService } from '../../../services/app-repository.service';
import { ModuleRef } from '@nestjs/core';
import { verify } from 'argon2';
import { Validator } from '../../../common/validations/validator';
import { JwtService } from '@nestjs/jwt';

@CommandHandler(SignInCommand)
export class SignInHandler extends AbstractHandler implements ICommandHandler<SignInCommand, StdResponse<SignInDto>> {
  constructor(repositoryService: AppRepositoryService, moduleRef: ModuleRef, private jwtService: JwtService) {
    super(repositoryService, moduleRef);
  }

  async execute(command: SignInCommand): Promise<StdResponse<SignInDto>> {
    const validationResult = await Validator.validate(command);
    if (validationResult.failed()) {
      return BadRequest<any>(validationResult.getMessage());
    }

    const admin = await this.repositoryService.admins
      .createQueryBuilder()
      .where('phone = :phone', { phone: command.phone })
      .getOne();

    if (!admin) {
      return BadRequestMsg('Invalid phone number or password');
    }

    if (!(await verify(admin.password, command.password))) {
      return BadRequestMsg('Invalid phone number or password');
    }

    const accessToken = await this.jwtService.signAsync(
      {
        id: admin.id,
        phone: admin.phone,
        table: 'admins',
      },
      {
        secret: process.env.JWT_SECRET,
        expiresIn: 60 * 60 * 24 * 365,
      },
    );

    return Ok(new SignInDto(admin.id, admin.phone, admin.fullName, accessToken));
  }
}
