import { ApiProperty } from '@nestjs/swagger';

export class SignInDto {
  @ApiProperty()
  public id: number;
  @ApiProperty()
  public phone: string;
  @ApiProperty()
  public fullName: string;
  @ApiProperty()
  public token: string;

  constructor(id: number, phone: string, fullName: string, token: string) {
    this.id = id;
    this.phone = phone;
    this.fullName = fullName;
    this.token = token;
  }
}
