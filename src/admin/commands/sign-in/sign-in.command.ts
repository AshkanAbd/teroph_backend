import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, isPhoneNumber, IsString, maxLength, minLength } from 'class-validator';
import { MustSync, NotNull } from '../../../common/validations/custom-validation/must-sync.rule';

export class SignInCommand {
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  @MustSync((_, y) => isPhoneNumber(y, 'IR'), NotNull, { message: '' })
  public phone: string;
  @ApiProperty()
  @IsNotEmpty({ message: '' })
  @IsString({ message: '' })
  @MustSync((_, x) => minLength(x, 8), NotNull, { message: '' })
  @MustSync((_, x) => maxLength(x, 100), NotNull, { message: '' })
  public password: string;
}
