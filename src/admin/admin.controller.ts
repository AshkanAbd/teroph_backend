import { Body, Controller, Post, Res, UsePipes } from '@nestjs/common';
import { AbstractController } from '../common/extensions/abstract-controller';
import { ApiStdResponse } from '../common/swagger/ApiStdResponse';
import { SignInDto } from './commands/sign-in/sign-in.dto';
import { ApiBearerAuth, ApiBody, ApiExtraModels, ApiTags } from '@nestjs/swagger';
import { SignInCommand } from './commands/sign-in/sign-in.command';
import { TransformerPipe } from '../common/pipes/transformer.pipe';
import { Response } from 'express';
import { CommandBus, QueryBus } from '@nestjs/cqrs';

@ApiBearerAuth()
@ApiTags('admin')
@Controller('admin')
export class AdminController extends AbstractController {
  constructor(private queryBus: QueryBus, private commandBus: CommandBus) {
    super();
  }

  @Post('signIn')
  @ApiBody({ type: SignInCommand })
  @ApiStdResponse(SignInDto)
  @ApiExtraModels(SignInDto)
  @UsePipes(new TransformerPipe('body'))
  async signIn(@Body() signInCommand: SignInCommand, @Res() res: Response) {
    this.base(res, await this.commandBus.execute(signInCommand));
  }
}
