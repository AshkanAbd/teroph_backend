import { Module } from '@nestjs/common';
import { AdminController } from './admin.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Entities } from '../app.entities';
import { CqrsModule } from '@nestjs/cqrs';
import { AppRepositoryService } from '../services/app-repository.service';
import { CommandHandlers } from './commands';
import { QueryHandlers } from './queries';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [
    TypeOrmModule.forFeature([...Entities]),
    CqrsModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
    }),
  ],
  controllers: [AdminController],
  providers: [AppRepositoryService, ...CommandHandlers, ...QueryHandlers],
})
export class AdminModule {
  //
}
