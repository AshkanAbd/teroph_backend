import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class updateVisit1693149578006 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('visits', [
      new TableColumn({
        name: 'week_1',
        type: 'int',
        isNullable: true,
      }),
      new TableColumn({
        name: 'week_2',
        type: 'int',
        isNullable: true,
      }),
      new TableColumn({
        name: 'week_3',
        type: 'int',
        isNullable: true,
      }),
      new TableColumn({
        name: 'week_4',
        type: 'int',
        isNullable: true,
      }),
      new TableColumn({
        name: 'month_2',
        type: 'int',
        isNullable: true,
      }),
      new TableColumn({
        name: 'month_6',
        type: 'int',
        isNullable: true,
      }),
      new TableColumn({
        name: 'month_12',
        type: 'int',
        isNullable: true,
      }),
      new TableColumn({
        name: 'cotroceptive_method_during_follow_up',
        type: 'text',
        isNullable: true,
      }),
      new TableColumn({
        name: 'hysterectomy',
        type: 'boolean',
        isNullable: true,
      }),
      new TableColumn({
        name: 'chemotrophy',
        type: 'boolean',
        isNullable: true,
      }),
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
