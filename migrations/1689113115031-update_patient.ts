import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class updatePatient1689113115031 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'patients',
      'phone',
      new TableColumn({
        name: 'phone',
        type: 'text',
        isNullable: true,
      }),
    );
    await queryRunner.changeColumn(
      'patients',
      'firstname',
      new TableColumn({
        name: 'firstname',
        type: 'text',
        isNullable: true,
      }),
    );
    await queryRunner.changeColumn(
      'patients',
      'lastname',
      new TableColumn({
        name: 'lastname',
        type: 'text',
        isNullable: true,
      }),
    );
    await queryRunner.changeColumn(
      'patients',
      'father_name',
      new TableColumn({
        name: 'father_name',
        type: 'text',
        isNullable: true,
      }),
    );
    await queryRunner.changeColumn(
      'patients',
      'national_id',
      new TableColumn({
        name: 'national_id',
        type: 'text',
        isNullable: true,
      }),
    );
    await queryRunner.changeColumn(
      'patients',
      'record_number',
      new TableColumn({
        name: 'record_number',
        type: 'text',
        isNullable: true,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
