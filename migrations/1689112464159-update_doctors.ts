import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class updateDoctors1689112464159 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'doctors',
      'full_name',
      new TableColumn({
        name: 'full_name',
        type: 'text',
        isNullable: true,
      }),
    );
    await queryRunner.changeColumn(
      'doctors',
      'medical_number',
      new TableColumn({
        name: 'medical_number',
        type: 'text',
        isNullable: true,
      }),
    );
    await queryRunner.changeColumn(
      'doctors',
      'speciality',
      new TableColumn({
        name: 'speciality',
        type: 'text',
        isNullable: true,
      }),
    );
    await queryRunner.changeColumn(
      'doctors',
      'referring_doctor',
      new TableColumn({
        name: 'referring_doctor',
        type: 'text',
        isNullable: true,
      }),
    );
    await queryRunner.changeColumn(
      'doctors',
      'referring_medical_number',
      new TableColumn({
        name: 'referring_medical_number',
        type: 'text',
        isNullable: true,
      }),
    );
    await queryRunner.changeColumn(
      'doctors',
      'referring_doctor_hospital',
      new TableColumn({
        name: 'referring_doctor_hospital',
        type: 'text',
        isNullable: true,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
