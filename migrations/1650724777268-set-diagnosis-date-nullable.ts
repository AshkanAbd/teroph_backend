import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class setDiagnosisDateNullable1650724777268 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'visits',
      new TableColumn({
        name: 'diagnosis_date',
        type: 'timestamp',
        isNullable: false,
      }),
      new TableColumn({
        name: 'diagnosis_date',
        type: 'timestamp',
        isNullable: true,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'visits',
      new TableColumn({
        name: 'diagnosis_date',
        type: 'timestamp',
        isNullable: true,
      }),
      new TableColumn({
        name: 'diagnosis_date',
        type: 'timestamp',
        isNullable: false,
      }),
    );
  }
}
