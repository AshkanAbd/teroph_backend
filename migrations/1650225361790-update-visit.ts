import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class updateVisit1650225361790 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('visits', [
      new TableColumn({
        type: 'timestamp',
        name: 'visit_date',
        isNullable: true,
      }),
      new TableColumn({
        type: 'text',
        name: 'visit_number',
        isNullable: true,
      }),
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumns('visits', ['visit_date', 'visit_number']);
  }
}
