import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class visit1649577112772 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'visits',
        columns: [
          {
            name: 'id',
            type: 'bigint',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'patient_id',
            type: 'bigint',
          },
          {
            name: 'doctor_id',
            type: 'bigint',
          },
          {
            name: 'gravida',
            type: 'smallint',
          },
          {
            name: 'para',
            type: 'smallint',
          },
          {
            name: 'twin',
            type: 'boolean',
          },
          {
            name: 'number_of_live_birth',
            type: 'varchar',
            length: '20',
          },
          {
            name: 'abortion',
            type: 'varchar',
            length: '20',
          },
          {
            name: 'cause_of_abortion',
            type: 'text',
          },
          {
            name: 'lmp',
            type: 'timestamp',
          },
          {
            name: 'gestational_age_weeks',
            type: 'smallint',
          },
          {
            name: 'gestational_age_days',
            type: 'smallint',
          },
          {
            name: 'previous_hydatidiform_mole',
            type: 'boolean',
          },
          {
            name: 'family_previous_hydatidiform_mole',
            type: 'boolean',
          },
          {
            name: 'contraceptive_drug_usage',
            type: 'boolean',
          },
          {
            name: 'events_leading_to_diagnosis',
            type: 'text',
          },
          {
            name: 'diagnosis_code',
            type: 'text',
          },
          {
            name: 'diagnosis_date',
            type: 'timestamp',
          },
          {
            name: 'comorbidity',
            type: 'text',
          },
          {
            name: 'figo_staging',
            type: 'smallint',
          },
          {
            name: 'figo_scoring',
            type: 'smallint',
          },
          {
            name: 'number_of_metastasis',
            type: 'varchar',
            length: '100',
          },
          {
            name: 'metastasis_location',
            type: 'varchar',
            length: '100',
          },
          {
            name: 'maximum_tumor_size',
            type: 'varchar',
            length: '20',
          },
          {
            name: 'method_of_evacuation',
            type: 'varchar',
            length: '30',
          },
          {
            name: 'date_of_evacuation',
            type: 'timestamp',
          },
          {
            name: 'classification_of_mole',
            type: 'varchar',
            length: '30',
          },
          {
            name: 'events_leading_to_diagnosis1',
            type: 'text',
          },
          {
            name: 'bhcg_level_at_diagnosis',
            type: 'text',
          },
          {
            name: 'gestational_age',
            type: 'smallint',
          },
          {
            name: 'uterine_size',
            type: 'int',
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()',
            isNullable: false,
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()',
            isNullable: false,
          },
          {
            name: 'deleted_at',
            type: 'timestamp',
            isNullable: true,
          },
        ],
        foreignKeys: [
          {
            columnNames: ['doctor_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'doctors',
            onDelete: 'CASCADE',
          },
          {
            columnNames: ['patient_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'patients',
            onDelete: 'CASCADE',
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('visits');
  }
}
