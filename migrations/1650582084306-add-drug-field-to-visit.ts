import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class addDrugFieldToVisit1650582084306 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('visits', [
      new TableColumn({
        name: 'how_long_drug_used',
        type: 'text',
        isNullable: true,
      }),
      new TableColumn({
        name: 'name_of_drug',
        type: 'text',
        isNullable: true,
      }),
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumns('visits', ['name_of_drug', 'how_long_drug_used']);
  }
}
