import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class doctor1648946638417 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'doctors',
        columns: [
          {
            name: 'id',
            type: 'bigint',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'full_name',
            type: 'varchar',
            length: '100',
            isNullable: false,
          },
          {
            name: 'medical_number',
            type: 'varchar',
            length: '20',
            isNullable: false,
          },
          {
            name: 'referring_doctor',
            type: 'varchar',
            length: '100',
            isNullable: false,
          },
          {
            name: 'attending_doctor',
            type: 'varchar',
            length: '100',
            isNullable: false,
          },
          {
            name: 'attending_doctor_specialty',
            type: 'varchar',
            length: '100',
            isNullable: false,
          },
          {
            name: 'created_at',
            type: 'timestamp',
            isNullable: false,
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            isNullable: false,
            default: 'now()',
          },
          {
            name: 'deleted_at',
            type: 'timestamp',
            isNullable: true,
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('doctors');
  }
}
