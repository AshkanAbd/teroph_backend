import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class Patient1648264543498 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'patients',
        columns: [
          {
            name: 'id',
            isPrimary: true,
            type: 'bigint',
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'phone',
            type: 'varchar',
            length: '11',
            isNullable: true,
          },
          {
            name: 'firstname',
            type: 'varchar',
            length: '50',
            isNullable: true,
          },
          {
            name: 'lastname',
            type: 'varchar',
            length: '100',
            isNullable: true,
          },
          {
            name: 'father_name',
            type: 'varchar',
            length: '50',
            isNullable: true,
          },
          {
            name: 'national_id',
            type: 'varchar',
            length: '10',
            isNullable: true,
          },
          {
            name: 'record_number',
            type: 'varchar',
            length: '50',
            isNullable: true,
          },
          {
            name: 'birth_date',
            type: 'timestamp',
            isNullable: true,
          },
          {
            name: 'address',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'diseases_age',
            type: 'int',
            isNullable: true,
          },
          {
            name: 'created_at',
            type: 'timestamp',
            isNullable: false,
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            isNullable: false,
            default: 'now()',
          },
          {
            name: 'deleted_at',
            type: 'timestamp',
            isNullable: true,
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('patients');
  }
}
