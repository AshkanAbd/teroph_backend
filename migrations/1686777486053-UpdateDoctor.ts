import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class UpdateDoctor1686777486053 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('doctors', 'attending_doctor');
    await queryRunner.dropColumn('doctors', 'attending_doctor_specialty');
    await queryRunner.query(`ALTER TABLE "doctors" ADD "speciality" varchar(255) NOT NULL DEFAULT ''`);
    await queryRunner.addColumn(
      'doctors',
      new TableColumn({
        name: 'referring_medical_number',
        type: 'varchar',
        isNullable: true,
        length: '255',
      }),
    );
    await queryRunner.addColumn(
      'doctors',
      new TableColumn({
        name: 'referring_doctor_hospital',
        type: 'varchar',
        isNullable: true,
        length: '255',
      }),
    );
    await queryRunner.changeColumn(
      'doctors',
      'referring_doctor',
      new TableColumn({
        isNullable: true,
        name: 'referring_doctor',
        type: 'varchar',
        length: '255',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('doctors', 'speciality');
    await queryRunner.dropColumn('doctors', 'referring_medical_number');
    await queryRunner.dropColumn('doctors', 'referring_doctor_hospital');
  }
}
