import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class updateVisit1689114460411 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'visits',
      'family_previous_hydatidiform_mole',
      new TableColumn({
        name: 'family_previous_hydatidiform_mole',
        type: 'text',
        isNullable: true,
      }),
    );
    await queryRunner.changeColumn(
      'visits',
      'previous_hydatidiform_mole',
      new TableColumn({
        name: 'previous_hydatidiform_mole',
        type: 'text',
        isNullable: true,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
