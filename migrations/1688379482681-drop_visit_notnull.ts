import { MigrationInterface, QueryRunner } from 'typeorm';

export class dropVisitNotnull1688379482681 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`alter table "visits" alter column "events_leading_to_diagnosis" drop NOT NULL`);
    await queryRunner.query(`alter table "visits" alter column "diagnosis_code" drop NOT NULL`);
    await queryRunner.query(`alter table "visits" alter column "diagnosis_date" drop NOT NULL`);
    await queryRunner.query(`alter table "visits" alter column "comorbidity" drop NOT NULL`);
    await queryRunner.query(`alter table "visits" alter column "figo_staging" drop NOT NULL`);
    await queryRunner.query(`alter table "visits" alter column "figo_scoring" drop NOT NULL`);
    await queryRunner.query(`alter table "visits" alter column "number_of_metastasis" drop NOT NULL`);
    await queryRunner.query(`alter table "visits" alter column "metastasis_location" drop NOT NULL`);
    await queryRunner.query(`alter table "visits" alter column "maximum_tumor_size" drop NOT NULL`);
    await queryRunner.query(`alter table "visits" alter column "method_of_evacuation" drop NOT NULL`);
    await queryRunner.query(`alter table "visits" alter column "date_of_evacuation" drop NOT NULL`);
    await queryRunner.query(`alter table "visits" alter column "classification_of_mole" drop NOT NULL;`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
