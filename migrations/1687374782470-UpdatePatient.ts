import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class UpdatePatient1687374782470 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'visits',
      'contraceptive_drug_usage',
      new TableColumn({
        name: 'contraceptive_drug_usage',
        type: 'text',
        isNullable: true,
      }),
    );
    await queryRunner.changeColumn(
      'visits',
      'gestational_age',
      new TableColumn({
        name: 'gestational_age',
        type: 'smallint',
        isNullable: true,
      }),
    );
    await queryRunner.changeColumn(
      'visits',
      'uterine_size',
      new TableColumn({
        name: 'uterine_size',
        type: 'int',
        isNullable: true,
      }),
    );
    await queryRunner.changeColumn(
      'visits',
      'cause_of_abortion',
      new TableColumn({
        name: 'cause_of_abortion',
        type: 'text',
        isNullable: true,
      }),
    );
    await queryRunner.changeColumn(
      'visits',
      'lmp',
      new TableColumn({
        name: 'lmp',
        type: 'timestamp',
        isNullable: true,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
