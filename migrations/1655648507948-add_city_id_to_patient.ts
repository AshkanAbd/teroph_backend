import { MigrationInterface, QueryRunner, TableColumn, TableForeignKey } from 'typeorm';

export class addCityIdToPatient1655648507948 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'patients',
      new TableColumn({
        name: 'city_id',
        type: 'bigint',
        isNullable: true,
      }),
    );
    await queryRunner.createForeignKey(
      'patients',
      new TableForeignKey({
        columnNames: ['city_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'cities',
        onDelete: 'CASCADE',
        name: 'patients_city_id',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('patients', 'patients_city_id');
    await queryRunner.dropColumn('patients', 'city_id');
  }
}
