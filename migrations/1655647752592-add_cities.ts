import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class addCities1655647752592 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'cities',
        columns: [
          {
            name: 'id',
            type: 'bigint',
            isGenerated: true,
            isPrimary: true,
            generationStrategy: 'increment',
          },
          {
            name: 'name',
            type: 'varchar',
            length: '200',
          },
          {
            name: 'state_id',
            type: 'bigint',
          },
        ],
        foreignKeys: [
          {
            columnNames: ['state_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'states',
            onDelete: 'CASCADE',
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('cities');
  }
}
